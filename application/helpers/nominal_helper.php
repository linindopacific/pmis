<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
if (!function_exists('nominal')) {
	function nominal($angka){
		$jd = number_format($angka, 0, ',', '.');
		return 'Rp. '.$jd;
	}

	function angka($angka){
		$d = number_format($angka, 0, ',', '.');
		return $d;
	}
}