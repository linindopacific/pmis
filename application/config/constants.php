<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
/*====================================
=            TABLE DB-NAV            =
====================================*/

define('NAV_TABLE_PAZ','Paz Ace Indonesia$');
define('NAV_TABLE_PMP','Persada MultiParts$');
define('NAV_TABLE_DLT','Dwikarya Linindo Teknik$');
define('NAV_TABLE_LPI','Linindo Pacific International$');
define('NAV_TABLE_MTI','Multipedia Teknika Indonesia $');
define('NAV_TABLE_LTI','Linindo Teknologi Int_$');
define('NAV_TABLE_TASI','Total Artha Solusi Indonesia$');
define('NAV_TABLE_ARN','Aruna International$');
define('NAV_TABLE_GSN','Geo Spatial Nusantara$');
define('NAV_TABLE_ASI','Allightsykes Indonesia$');
define('NAV_TABLE_JMN','Jomon Persada Nusantara$');
define('NAV_TABLE_ABC','Amarta Bayu Caraka$');

define('NAV_TABLE_3','Payment Terms'); // Payment Terms
define('NAV_TABLE_4','Currency'); // Currency
define('NAV_TABLE_5','Finance Charge Terms'); // Finance Charge Terms
define('NAV_TABLE_6','Customer Price Group'); // Customer Price Group
define('NAV_TABLE_7','Standard Text'); // Standard Text
define('NAV_TABLE_8','Language'); // Language
define('NAV_TABLE_9','Country_Region'); // Country/Region
define('NAV_TABLE_10','Shipment Method'); // Shipment Method
define('NAV_TABLE_13','Salesperson_Purchaser'); // Salesperson/Purchaser
define('NAV_TABLE_14','Location'); // Location
define('NAV_TABLE_15','G_L Account'); // G/L Account
define('NAV_TABLE_17','G_L Entry'); // G/L Entry
define('NAV_TABLE_18','Customer'); // Customer
define('NAV_TABLE_19','Cust_ Invoice Disc_'); // Cust. Invoice Disc.
define('NAV_TABLE_21','Cust_ Ledger Entry'); // Cust. Ledger Entry
define('NAV_TABLE_23','Vendor'); // Vendor
define('NAV_TABLE_24','Vendor Invoice Disc_'); // Vendor Invoice Disc.
define('NAV_TABLE_25','Vendor Ledger Entry'); // Vendor Ledger Entry
define('NAV_TABLE_27','Item'); // Item
define('NAV_TABLE_30','Item Translation'); // Item Translation
define('NAV_TABLE_32','Item Ledger Entry'); // Item Ledger Entry
define('NAV_TABLE_36','Sales Header'); // Sales Header
define('NAV_TABLE_37','Sales Line'); // Sales Line
define('NAV_TABLE_38','Purchase Header'); // Purchase Header
define('NAV_TABLE_39','Purchase Line'); // Purchase Line
define('NAV_TABLE_42','Rounding Method'); // Rounding Method
define('NAV_TABLE_43','Purch_ Comment Line'); // Purch. Comment Line
define('NAV_TABLE_44','Sales Comment Line'); // Sales Comment Line
define('NAV_TABLE_45','G_L Register'); // G/L Register
define('NAV_TABLE_46','Item Register'); // Item Register
define('NAV_TABLE_47','Aging Band Buffer'); // Aging Band Buffer
define('NAV_TABLE_48','Invt_ Posting Buffer'); // Invt. Posting Buffer
define('NAV_TABLE_49','Invoice Post_ Buffer'); // Invoice Post. Buffer
define('NAV_TABLE_50','Accounting Period'); // Accounting Period
define('NAV_TABLE_51','User Time Register'); // User Time Register
define('NAV_TABLE_60','Document Sending Profile'); // Document Sending Profile
define('NAV_TABLE_61','Electronic Document Format'); // Electronic Document Format
define('NAV_TABLE_62','Record Export Buffer'); // Record Export Buffer
define('NAV_TABLE_77','Report Selections'); // Report Selections
define('NAV_TABLE_78','Printer Selection'); // Printer Selection
define('NAV_TABLE_79','Company Information'); // Company Information
define('NAV_TABLE_80','Gen_ Journal Template'); // Gen. Journal Template
define('NAV_TABLE_81','Gen_ Journal Line'); // Gen. Journal Line
define('NAV_TABLE_82','Item Journal Template'); // Item Journal Template
define('NAV_TABLE_83','Item Journal Line'); // Item Journal Line
define('NAV_TABLE_84','Acc_ Schedule Name'); // Acc. Schedule Name
define('NAV_TABLE_85','Acc_ Schedule Line'); // Acc. Schedule Line
define('NAV_TABLE_86','Exch_ Rate Adjmt_ Reg_'); // Exch. Rate Adjmt. Reg.
define('NAV_TABLE_87','Date Compr_ Register'); // Date Compr. Register
define('NAV_TABLE_90','BOM Component'); // BOM Component
define('NAV_TABLE_91','User Setup'); // User Setup
define('NAV_TABLE_92','Customer Posting Group'); // Customer Posting Group
define('NAV_TABLE_93','Vendor Posting Group'); // Vendor Posting Group
define('NAV_TABLE_94','Inventory Posting Group'); // Inventory Posting Group
define('NAV_TABLE_95','G_L Budget Name'); // G/L Budget Name
define('NAV_TABLE_96','G_L Budget Entry'); // G/L Budget Entry
define('NAV_TABLE_97','Comment Line'); // Comment Line
define('NAV_TABLE_98','General Ledger Setup'); // General Ledger Setup
define('NAV_TABLE_99','Item Vendor'); // Item Vendor
define('NAV_TABLE_110','Sales Shipment Header'); // Sales Shipment Header
define('NAV_TABLE_111','Sales Shipment Line'); // Sales Shipment Line
define('NAV_TABLE_112','Sales Invoice Header'); // Sales Invoice Header
define('NAV_TABLE_113','Sales Invoice Line'); // Sales Invoice Line
define('NAV_TABLE_114','Sales Cr_Memo Header'); // Sales Cr.Memo Header
define('NAV_TABLE_115','Sales Cr_Memo Line'); // Sales Cr.Memo Line
define('NAV_TABLE_120','Purch_ Rcpt_ Header'); // Purch. Rcpt. Header
define('NAV_TABLE_121','Purch_ Rcpt_ Line'); // Purch. Rcpt. Line
define('NAV_TABLE_122','Purch_ Inv_ Header'); // Purch. Inv. Header
define('NAV_TABLE_123','Purch_ Inv_ Line'); // Purch. Inv. Line
define('NAV_TABLE_124','Purch_ Cr_ Memo Hdr_'); // Purch. Cr. Memo Hdr.
define('NAV_TABLE_125','Purch_ Cr_ Memo Line'); // Purch. Cr. Memo Line
define('NAV_TABLE_130','Incoming Document'); // Incoming Document
define('NAV_TABLE_131','Incoming Documents Setup'); // Incoming Documents Setup
define('NAV_TABLE_132','Incoming Document Approver'); // Incoming Document Approver
define('NAV_TABLE_133','Incoming Document Attachment'); // Incoming Document Attachment
define('NAV_TABLE_134','Posted Docs_ With No Inc_ Buf_'); // Posted Docs. With No Inc. Buf.
define('NAV_TABLE_135','Acc_ Sched_ KPI Web Srv_ Setup'); // Acc. Sched. KPI Web Srv. Setup
define('NAV_TABLE_136','Acc_ Sched_ KPI Web Srv_ Line'); // Acc. Sched. KPI Web Srv. Line
define('NAV_TABLE_137','Inc_ Doc_ Attachment Overview'); // Inc. Doc. Attachment Overview
define('NAV_TABLE_140','License Agreement'); // License Agreement
define('NAV_TABLE_152','Resource Group'); // Resource Group
define('NAV_TABLE_156','Resource'); // Resource
define('NAV_TABLE_160','Res_ Capacity Entry'); // Res. Capacity Entry
define('NAV_TABLE_167','Job'); // Job
define('NAV_TABLE_169','Job Ledger Entry'); // Job Ledger Entry
define('NAV_TABLE_170','Standard Sales Code'); // Standard Sales Code
define('NAV_TABLE_171','Standard Sales Line'); // Standard Sales Line
define('NAV_TABLE_172','Standard Customer Sales Code'); // Standard Customer Sales Code
define('NAV_TABLE_173','Standard Purchase Code'); // Standard Purchase Code
define('NAV_TABLE_174','Standard Purchase Line'); // Standard Purchase Line
define('NAV_TABLE_175','Standard Vendor Purchase Code'); // Standard Vendor Purchase Code
define('NAV_TABLE_179','Reversal Entry'); // Reversal Entry
define('NAV_TABLE_180','G_L Account Where-Used'); // G/L Account Where-Used
define('NAV_TABLE_200','Work Type'); // Work Type
define('NAV_TABLE_201','Resource Price'); // Resource Price
define('NAV_TABLE_202','Resource Cost'); // Resource Cost
define('NAV_TABLE_203','Res_ Ledger Entry'); // Res. Ledger Entry
define('NAV_TABLE_204','Unit of Measure'); // Unit of Measure
define('NAV_TABLE_205','Resource Unit of Measure'); // Resource Unit of Measure
define('NAV_TABLE_206','Res_ Journal Template'); // Res. Journal Template
define('NAV_TABLE_207','Res_ Journal Line'); // Res. Journal Line
define('NAV_TABLE_208','Job Posting Group'); // Job Posting Group
define('NAV_TABLE_209','Job Journal Template'); // Job Journal Template
define('NAV_TABLE_210','Job Journal Line'); // Job Journal Line
define('NAV_TABLE_212','Job Posting Buffer'); // Job Posting Buffer
define('NAV_TABLE_220','Business Unit'); // Business Unit
define('NAV_TABLE_221','Gen_ Jnl_ Allocation'); // Gen. Jnl. Allocation
define('NAV_TABLE_222','Ship-to Address'); // Ship-to Address
define('NAV_TABLE_223','Drop Shpt_ Post_ Buffer'); // Drop Shpt. Post. Buffer
define('NAV_TABLE_224','Order Address'); // Order Address
define('NAV_TABLE_225','Post Code'); // Post Code
define('NAV_TABLE_230','Source Code'); // Source Code
define('NAV_TABLE_231','Reason Code'); // Reason Code
define('NAV_TABLE_232','Gen_ Journal Batch'); // Gen. Journal Batch
define('NAV_TABLE_233','Item Journal Batch'); // Item Journal Batch
define('NAV_TABLE_236','Res_ Journal Batch'); // Res. Journal Batch
define('NAV_TABLE_237','Job Journal Batch'); // Job Journal Batch
define('NAV_TABLE_240','Resource Register'); // Resource Register
define('NAV_TABLE_241','Job Register'); // Job Register
define('NAV_TABLE_242','Source Code Setup'); // Source Code Setup
define('NAV_TABLE_244','Req_ Wksh_ Template'); // Req. Wksh. Template
define('NAV_TABLE_245','Requisition Wksh_ Name'); // Requisition Wksh. Name
define('NAV_TABLE_246','Requisition Line'); // Requisition Line
define('NAV_TABLE_249','VAT Registration Log'); // VAT Registration Log
define('NAV_TABLE_250','Gen_ Business Posting Group'); // Gen. Business Posting Group
define('NAV_TABLE_251','Gen_ Product Posting Group'); // Gen. Product Posting Group
define('NAV_TABLE_252','General Posting Setup'); // General Posting Setup
define('NAV_TABLE_253','G_L Entry - VAT Entry Link'); // G/L Entry - VAT Entry Link
define('NAV_TABLE_254','VAT Entry'); // VAT Entry
define('NAV_TABLE_255','VAT Statement Template'); // VAT Statement Template
define('NAV_TABLE_256','VAT Statement Line'); // VAT Statement Line
define('NAV_TABLE_257','VAT Statement Name'); // VAT Statement Name
define('NAV_TABLE_258','Transaction Type'); // Transaction Type
define('NAV_TABLE_259','Transport Method'); // Transport Method
define('NAV_TABLE_260','Tariff Number'); // Tariff Number
define('NAV_TABLE_261','Intrastat Jnl_ Template'); // Intrastat Jnl. Template
define('NAV_TABLE_262','Intrastat Jnl_ Batch'); // Intrastat Jnl. Batch
define('NAV_TABLE_263','Intrastat Jnl_ Line'); // Intrastat Jnl. Line
define('NAV_TABLE_264','Currency Amount'); // Currency Amount
define('NAV_TABLE_265','Document Entry'); // Document Entry
define('NAV_TABLE_266','Customer Amount'); // Customer Amount
define('NAV_TABLE_267','Vendor Amount'); // Vendor Amount
define('NAV_TABLE_268','Item Amount'); // Item Amount
define('NAV_TABLE_269','G_L Account Net Change'); // G/L Account Net Change
define('NAV_TABLE_270','Bank Account'); // Bank Account
define('NAV_TABLE_271','Bank Account Ledger Entry'); // Bank Account Ledger Entry
define('NAV_TABLE_272','Check Ledger Entry'); // Check Ledger Entry
define('NAV_TABLE_273','Bank Acc_ Reconciliation'); // Bank Acc. Reconciliation
define('NAV_TABLE_274','Bank Acc_ Reconciliation Line'); // Bank Acc. Reconciliation Line
define('NAV_TABLE_275','Bank Account Statement'); // Bank Account Statement
define('NAV_TABLE_276','Bank Account Statement Line'); // Bank Account Statement Line
define('NAV_TABLE_277','Bank Account Posting Group'); // Bank Account Posting Group
define('NAV_TABLE_278','Job Journal Quantity'); // Job Journal Quantity
define('NAV_TABLE_279','Extended Text Header'); // Extended Text Header
define('NAV_TABLE_280','Extended Text Line'); // Extended Text Line
define('NAV_TABLE_281','Phys_ Inventory Ledger Entry'); // Phys. Inventory Ledger Entry
define('NAV_TABLE_282','Entry_Exit Point'); // Entry/Exit Point
define('NAV_TABLE_283','Line Number Buffer'); // Line Number Buffer
define('NAV_TABLE_284','Area'); // Area
define('NAV_TABLE_285','Transaction Specification'); // Transaction Specification
define('NAV_TABLE_286','Territory'); // Territory
define('NAV_TABLE_287','Customer Bank Account'); // Customer Bank Account
define('NAV_TABLE_288','Vendor Bank Account'); // Vendor Bank Account
define('NAV_TABLE_289','Payment Method'); // Payment Method
define('NAV_TABLE_290','VAT Amount Line'); // VAT Amount Line
define('NAV_TABLE_291','Shipping Agent'); // Shipping Agent
define('NAV_TABLE_292','Reminder Terms'); // Reminder Terms
define('NAV_TABLE_293','Reminder Level'); // Reminder Level
define('NAV_TABLE_294','Reminder Text'); // Reminder Text
define('NAV_TABLE_295','Reminder Header'); // Reminder Header
define('NAV_TABLE_296','Reminder Line'); // Reminder Line
define('NAV_TABLE_297','Issued Reminder Header'); // Issued Reminder Header
define('NAV_TABLE_298','Issued Reminder Line'); // Issued Reminder Line
define('NAV_TABLE_299','Reminder Comment Line'); // Reminder Comment Line
define('NAV_TABLE_300','Reminder_Fin_ Charge Entry'); // Reminder/Fin. Charge Entry
define('NAV_TABLE_301','Finance Charge Text'); // Finance Charge Text
define('NAV_TABLE_302','Finance Charge Memo Header'); // Finance Charge Memo Header
define('NAV_TABLE_303','Finance Charge Memo Line'); // Finance Charge Memo Line
define('NAV_TABLE_304','Issued Fin_ Charge Memo Header'); // Issued Fin. Charge Memo Header
define('NAV_TABLE_305','Issued Fin_ Charge Memo Line'); // Issued Fin. Charge Memo Line
define('NAV_TABLE_306','Fin_ Charge Comment Line'); // Fin. Charge Comment Line
define('NAV_TABLE_307','Inventory Buffer'); // Inventory Buffer
define('NAV_TABLE_308','No_ Series'); // No. Series
define('NAV_TABLE_309','No_ Series Line'); // No. Series Line
define('NAV_TABLE_310','No_ Series Relationship'); // No. Series Relationship
define('NAV_TABLE_311','Sales & Receivables Setup'); // Sales & Receivables Setup
define('NAV_TABLE_312','Purchases & Payables Setup'); // Purchases & Payables Setup
define('NAV_TABLE_313','Inventory Setup'); // Inventory Setup
define('NAV_TABLE_314','Resources Setup'); // Resources Setup
define('NAV_TABLE_315','Jobs Setup'); // Jobs Setup
define('NAV_TABLE_317','Payable Vendor Ledger Entry'); // Payable Vendor Ledger Entry
define('NAV_TABLE_318','Tax Area'); // Tax Area
define('NAV_TABLE_319','Tax Area Line'); // Tax Area Line
define('NAV_TABLE_320','Tax Jurisdiction'); // Tax Jurisdiction
define('NAV_TABLE_321','Tax Group'); // Tax Group
define('NAV_TABLE_322','Tax Detail'); // Tax Detail
define('NAV_TABLE_323','VAT Business Posting Group'); // VAT Business Posting Group
define('NAV_TABLE_324','VAT Product Posting Group'); // VAT Product Posting Group
define('NAV_TABLE_325','VAT Posting Setup'); // VAT Posting Setup
define('NAV_TABLE_328','Currency for Fin_ Charge Terms'); // Currency for Fin. Charge Terms
define('NAV_TABLE_329','Currency for Reminder Level'); // Currency for Reminder Level
define('NAV_TABLE_330','Currency Exchange Rate'); // Currency Exchange Rate
define('NAV_TABLE_331','Adjust Exchange Rate Buffer'); // Adjust Exchange Rate Buffer
define('NAV_TABLE_332','Currency Total Buffer'); // Currency Total Buffer
define('NAV_TABLE_333','Column Layout Name'); // Column Layout Name
define('NAV_TABLE_334','Column Layout'); // Column Layout
define('NAV_TABLE_335','Resource Price Change'); // Resource Price Change
define('NAV_TABLE_336','Tracking Specification'); // Tracking Specification
define('NAV_TABLE_337','Reservation Entry'); // Reservation Entry
define('NAV_TABLE_338','Entry Summary'); // Entry Summary
define('NAV_TABLE_339','Item Application Entry'); // Item Application Entry
define('NAV_TABLE_340','Customer Discount Group'); // Customer Discount Group
define('NAV_TABLE_341','Item Discount Group'); // Item Discount Group
define('NAV_TABLE_342','Acc_ Sched_ Cell Value'); // Acc. Sched. Cell Value
define('NAV_TABLE_343','Item Application Entry History'); // Item Application Entry History
define('NAV_TABLE_347','Close Income Statement Buffer'); // Close Income Statement Buffer
define('NAV_TABLE_348','Dimension'); // Dimension
define('NAV_TABLE_349','Dimension Value'); // Dimension Value
define('NAV_TABLE_350','Dimension Combination'); // Dimension Combination
define('NAV_TABLE_351','Dimension Value Combination'); // Dimension Value Combination
define('NAV_TABLE_352','Default Dimension'); // Default Dimension
define('NAV_TABLE_353','Dimension ID Buffer'); // Dimension ID Buffer
define('NAV_TABLE_354','Default Dimension Priority'); // Default Dimension Priority
define('NAV_TABLE_360','Dimension Buffer'); // Dimension Buffer
define('NAV_TABLE_363','Analysis View'); // Analysis View
define('NAV_TABLE_364','Analysis View Filter'); // Analysis View Filter
define('NAV_TABLE_365','Analysis View Entry'); // Analysis View Entry
define('NAV_TABLE_366','Analysis View Budget Entry'); // Analysis View Budget Entry
define('NAV_TABLE_367','Dimension Code Buffer'); // Dimension Code Buffer
define('NAV_TABLE_368','Dimension Selection Buffer'); // Dimension Selection Buffer
define('NAV_TABLE_369','Selected Dimension'); // Selected Dimension
define('NAV_TABLE_370','Excel Buffer'); // Excel Buffer
define('NAV_TABLE_371','Budget Buffer'); // Budget Buffer
define('NAV_TABLE_372','Payment Buffer'); // Payment Buffer
define('NAV_TABLE_373','Dimension Entry Buffer'); // Dimension Entry Buffer
define('NAV_TABLE_374','G_L Acc_ Budget Buffer'); // G/L Acc. Budget Buffer
define('NAV_TABLE_375','Dimension Code Amount Buffer'); // Dimension Code Amount Buffer
define('NAV_TABLE_376','G_L Account (Analysis View)'); // G/L Account (Analysis View)
define('NAV_TABLE_377','Object Translation'); // Object Translation
define('NAV_TABLE_378','Report List Translation'); // Report List Translation
define('NAV_TABLE_379','Detailed Cust_ Ledg_ Entry'); // Detailed Cust. Ledg. Entry
define('NAV_TABLE_380','Detailed Vendor Ledg_ Entry'); // Detailed Vendor Ledg. Entry
define('NAV_TABLE_381','VAT Registration No_ Format'); // VAT Registration No. Format
define('NAV_TABLE_382','CV Ledger Entry Buffer'); // CV Ledger Entry Buffer
define('NAV_TABLE_383','Detailed CV Ledg_ Entry Buffer'); // Detailed CV Ledg. Entry Buffer
define('NAV_TABLE_384','Reconcile CV Acc Buffer'); // Reconcile CV Acc Buffer
define('NAV_TABLE_386','Entry No_ Amount Buffer'); // Entry No. Amount Buffer
define('NAV_TABLE_388','Dimension Translation'); // Dimension Translation
define('NAV_TABLE_390','Availability at Date'); // Availability at Date
define('NAV_TABLE_394','XBRL Taxonomy'); // XBRL Taxonomy
define('NAV_TABLE_395','XBRL Taxonomy Line'); // XBRL Taxonomy Line
define('NAV_TABLE_396','XBRL Comment Line'); // XBRL Comment Line
define('NAV_TABLE_397','XBRL G_L Map Line'); // XBRL G/L Map Line
define('NAV_TABLE_398','XBRL Rollup Line'); // XBRL Rollup Line
define('NAV_TABLE_399','XBRL Schema'); // XBRL Schema
define('NAV_TABLE_400','XBRL Linkbase'); // XBRL Linkbase
define('NAV_TABLE_401','XBRL Taxonomy Label'); // XBRL Taxonomy Label
define('NAV_TABLE_402','Change Log Setup'); // Change Log Setup
define('NAV_TABLE_403','Change Log Setup (Table)'); // Change Log Setup (Table)
define('NAV_TABLE_404','Change Log Setup (Field)'); // Change Log Setup (Field)
define('NAV_TABLE_405','Change Log Entry'); // Change Log Entry
define('NAV_TABLE_408','XBRL Line Constant'); // XBRL Line Constant
define('NAV_TABLE_409','SMTP Mail Setup'); // SMTP Mail Setup
define('NAV_TABLE_410','IC G_L Account'); // IC G/L Account
define('NAV_TABLE_411','IC Dimension'); // IC Dimension
define('NAV_TABLE_412','IC Dimension Value'); // IC Dimension Value
define('NAV_TABLE_413','IC Partner'); // IC Partner
define('NAV_TABLE_414','IC Outbox Transaction'); // IC Outbox Transaction
define('NAV_TABLE_415','IC Outbox Jnl_ Line'); // IC Outbox Jnl. Line
define('NAV_TABLE_416','Handled IC Outbox Trans_'); // Handled IC Outbox Trans.
define('NAV_TABLE_417','Handled IC Outbox Jnl_ Line'); // Handled IC Outbox Jnl. Line
define('NAV_TABLE_418','IC Inbox Transaction'); // IC Inbox Transaction
define('NAV_TABLE_419','IC Inbox Jnl_ Line'); // IC Inbox Jnl. Line
define('NAV_TABLE_420','Handled IC Inbox Trans_'); // Handled IC Inbox Trans.
define('NAV_TABLE_421','Handled IC Inbox Jnl_ Line'); // Handled IC Inbox Jnl. Line
define('NAV_TABLE_423','IC Inbox_Outbox Jnl_ Line Dim_'); // IC Inbox/Outbox Jnl. Line Dim.
define('NAV_TABLE_424','IC Comment Line'); // IC Comment Line
define('NAV_TABLE_426','IC Outbox Sales Header'); // IC Outbox Sales Header
define('NAV_TABLE_427','IC Outbox Sales Line'); // IC Outbox Sales Line
define('NAV_TABLE_428','IC Outbox Purchase Header'); // IC Outbox Purchase Header
define('NAV_TABLE_429','IC Outbox Purchase Line'); // IC Outbox Purchase Line
define('NAV_TABLE_430','Handled IC Outbox Sales Header'); // Handled IC Outbox Sales Header
define('NAV_TABLE_431','Handled IC Outbox Sales Line'); // Handled IC Outbox Sales Line
define('NAV_TABLE_432','Handled IC Outbox Purch_ Hdr'); // Handled IC Outbox Purch. Hdr
define('NAV_TABLE_433','Handled IC Outbox Purch_ Line'); // Handled IC Outbox Purch. Line
define('NAV_TABLE_434','IC Inbox Sales Header'); // IC Inbox Sales Header
define('NAV_TABLE_435','IC Inbox Sales Line'); // IC Inbox Sales Line
define('NAV_TABLE_436','IC Inbox Purchase Header'); // IC Inbox Purchase Header
define('NAV_TABLE_437','IC Inbox Purchase Line'); // IC Inbox Purchase Line
define('NAV_TABLE_438','Handled IC Inbox Sales Header'); // Handled IC Inbox Sales Header
define('NAV_TABLE_439','Handled IC Inbox Sales Line'); // Handled IC Inbox Sales Line
define('NAV_TABLE_440','Handled IC Inbox Purch_ Header'); // Handled IC Inbox Purch. Header
define('NAV_TABLE_441','Handled IC Inbox Purch_ Line'); // Handled IC Inbox Purch. Line
define('NAV_TABLE_442','IC Document Dimension'); // IC Document Dimension
define('NAV_TABLE_450','Bar Chart Buffer'); // Bar Chart Buffer
define('NAV_TABLE_454','Approval Entry'); // Approval Entry
define('NAV_TABLE_455','Approval Comment Line'); // Approval Comment Line
define('NAV_TABLE_456','Posted Approval Entry'); // Posted Approval Entry
define('NAV_TABLE_457','Posted Approval Comment Line'); // Posted Approval Comment Line
define('NAV_TABLE_458','Overdue Approval Entry'); // Overdue Approval Entry
define('NAV_TABLE_459','Sales Prepayment _'); // Sales Prepayment %
define('NAV_TABLE_460','Purchase Prepayment _'); // Purchase Prepayment %
define('NAV_TABLE_461','Prepayment Inv_ Line Buffer'); // Prepayment Inv. Line Buffer
define('NAV_TABLE_462','Payment Term Translation'); // Payment Term Translation
define('NAV_TABLE_463','Shipment Method Translation'); // Shipment Method Translation
define('NAV_TABLE_470','Job Queue'); // Job Queue
define('NAV_TABLE_471','Job Queue Category'); // Job Queue Category
define('NAV_TABLE_472','Job Queue Entry'); // Job Queue Entry
define('NAV_TABLE_474','Job Queue Log Entry'); // Job Queue Log Entry
define('NAV_TABLE_477','Report Inbox'); // Report Inbox
define('NAV_TABLE_480','Dimension Set Entry'); // Dimension Set Entry
define('NAV_TABLE_481','Dimension Set Tree Node'); // Dimension Set Tree Node
define('NAV_TABLE_482','Reclas_ Dimension Set Buffer'); // Reclas. Dimension Set Buffer
define('NAV_TABLE_485','Business Chart Buffer'); // Business Chart Buffer
define('NAV_TABLE_486','Business Chart Map'); // Business Chart Map
define('NAV_TABLE_487','Business Chart User Setup'); // Business Chart User Setup
define('NAV_TABLE_550','VAT Rate Change Setup'); // VAT Rate Change Setup
define('NAV_TABLE_551','VAT Rate Change Conversion'); // VAT Rate Change Conversion
define('NAV_TABLE_552','VAT Rate Change Log Entry'); // VAT Rate Change Log Entry
define('NAV_TABLE_560','VAT Clause'); // VAT Clause
define('NAV_TABLE_561','VAT Clause Translation'); // VAT Clause Translation
define('NAV_TABLE_700','Error Message'); // Error Message
define('NAV_TABLE_710','Activity Log'); // Activity Log
define('NAV_TABLE_740','VAT Report Header'); // VAT Report Header
define('NAV_TABLE_741','VAT Report Line'); // VAT Report Line
define('NAV_TABLE_743','VAT Report Setup'); // VAT Report Setup
define('NAV_TABLE_744','VAT Report Line Relation'); // VAT Report Line Relation
define('NAV_TABLE_745','VAT Report Error Log'); // VAT Report Error Log
define('NAV_TABLE_750','Standard General Journal'); // Standard General Journal
define('NAV_TABLE_751','Standard General Journal Line'); // Standard General Journal Line
define('NAV_TABLE_752','Standard Item Journal'); // Standard Item Journal
define('NAV_TABLE_753','Standard Item Journal Line'); // Standard Item Journal Line
define('NAV_TABLE_760','Trailing Sales Orders Setup'); // Trailing Sales Orders Setup
define('NAV_TABLE_762','Account Schedules Chart Setup'); // Account Schedules Chart Setup
define('NAV_TABLE_763','Acc_ Sched_ Chart Setup Line'); // Acc. Sched. Chart Setup Line
define('NAV_TABLE_770','Analysis Report Chart Setup'); // Analysis Report Chart Setup
define('NAV_TABLE_771','Analysis Report Chart Line'); // Analysis Report Chart Line
define('NAV_TABLE_780','Certificate of Supply'); // Certificate of Supply
define('NAV_TABLE_800','Online Map Setup'); // Online Map Setup
define('NAV_TABLE_801','Online Map Parameter Setup'); // Online Map Parameter Setup
define('NAV_TABLE_806','Geolocation'); // Geolocation
define('NAV_TABLE_823','Name_Value Buffer'); // Name/Value Buffer
define('NAV_TABLE_824','DO Payment Connection Details'); // DO Payment Connection Details
define('NAV_TABLE_825','DO Payment Connection Setup'); // DO Payment Connection Setup
define('NAV_TABLE_826','DO Payment Setup'); // DO Payment Setup
define('NAV_TABLE_827','DO Payment Credit Card'); // DO Payment Credit Card
define('NAV_TABLE_828','DO Payment Credit Card Number'); // DO Payment Credit Card Number
define('NAV_TABLE_829','DO Payment Trans_ Log Entry'); // DO Payment Trans. Log Entry
define('NAV_TABLE_830','DO Payment Card Type'); // DO Payment Card Type
define('NAV_TABLE_840','Cash Flow Forecast'); // Cash Flow Forecast
define('NAV_TABLE_841','Cash Flow Account'); // Cash Flow Account
define('NAV_TABLE_842','Cash Flow Account Comment'); // Cash Flow Account Comment
define('NAV_TABLE_843','Cash Flow Setup'); // Cash Flow Setup
define('NAV_TABLE_846','Cash Flow Worksheet Line'); // Cash Flow Worksheet Line
define('NAV_TABLE_847','Cash Flow Forecast Entry'); // Cash Flow Forecast Entry
define('NAV_TABLE_849','Cash Flow Manual Revenue'); // Cash Flow Manual Revenue
define('NAV_TABLE_850','Cash Flow Manual Expense'); // Cash Flow Manual Expense
define('NAV_TABLE_856','Cash Flow Report Selection'); // Cash Flow Report Selection
define('NAV_TABLE_869','Cash Flow Chart Setup'); // Cash Flow Chart Setup
define('NAV_TABLE_870','Social Listening Setup'); // Social Listening Setup
define('NAV_TABLE_871','Social Listening Search Topic'); // Social Listening Search Topic
define('NAV_TABLE_900','Assembly Header'); // Assembly Header
define('NAV_TABLE_901','Assembly Line'); // Assembly Line
define('NAV_TABLE_904','Assemble-to-Order Link'); // Assemble-to-Order Link
define('NAV_TABLE_905','Assembly Setup'); // Assembly Setup
define('NAV_TABLE_906','Assembly Comment Line'); // Assembly Comment Line
define('NAV_TABLE_910','Posted Assembly Header'); // Posted Assembly Header
define('NAV_TABLE_911','Posted Assembly Line'); // Posted Assembly Line
define('NAV_TABLE_914','Posted Assemble-to-Order Link'); // Posted Assemble-to-Order Link
define('NAV_TABLE_915','ATO Sales Buffer'); // ATO Sales Buffer
define('NAV_TABLE_950','Time Sheet Header'); // Time Sheet Header
define('NAV_TABLE_951','Time Sheet Line'); // Time Sheet Line
define('NAV_TABLE_952','Time Sheet Detail'); // Time Sheet Detail
define('NAV_TABLE_953','Time Sheet Comment Line'); // Time Sheet Comment Line
define('NAV_TABLE_954','Time Sheet Header Archive'); // Time Sheet Header Archive
define('NAV_TABLE_955','Time Sheet Line Archive'); // Time Sheet Line Archive
define('NAV_TABLE_956','Time Sheet Detail Archive'); // Time Sheet Detail Archive
define('NAV_TABLE_957','Time Sheet Cmt_ Line Archive'); // Time Sheet Cmt. Line Archive
define('NAV_TABLE_958','Time Sheet Posting Entry'); // Time Sheet Posting Entry
define('NAV_TABLE_959','Time Sheet Chart Setup'); // Time Sheet Chart Setup
define('NAV_TABLE_980','Payment Registration Setup'); // Payment Registration Setup
define('NAV_TABLE_981','Payment Registration Buffer'); // Payment Registration Buffer
define('NAV_TABLE_983','Document Search Result'); // Document Search Result
define('NAV_TABLE_1001','Job Task'); // Job Task
define('NAV_TABLE_1002','Job Task Dimension'); // Job Task Dimension
define('NAV_TABLE_1003','Job Planning Line'); // Job Planning Line
define('NAV_TABLE_1004','Job WIP Entry'); // Job WIP Entry
define('NAV_TABLE_1005','Job WIP G_L Entry'); // Job WIP G/L Entry
define('NAV_TABLE_1006','Job WIP Method'); // Job WIP Method
define('NAV_TABLE_1007','Job WIP Warning'); // Job WIP Warning
define('NAV_TABLE_1012','Job Resource Price'); // Job Resource Price
define('NAV_TABLE_1013','Job Item Price'); // Job Item Price
define('NAV_TABLE_1014','Job G_L Account Price'); // Job G/L Account Price
define('NAV_TABLE_1015','Job Entry No_'); // Job Entry No.
define('NAV_TABLE_1017','Job Buffer'); // Job Buffer
define('NAV_TABLE_1018','Job WIP Buffer'); // Job WIP Buffer
define('NAV_TABLE_1019','Job Difference Buffer'); // Job Difference Buffer
define('NAV_TABLE_1020','Job Usage Link'); // Job Usage Link
define('NAV_TABLE_1021','Job WIP Total'); // Job WIP Total
define('NAV_TABLE_1022','Job Planning Line Invoice'); // Job Planning Line Invoice
define('NAV_TABLE_1050','Additional Fee Setup'); // Additional Fee Setup
define('NAV_TABLE_1051','Sorting Table'); // Sorting Table
define('NAV_TABLE_1052','Reminder Terms Translation'); // Reminder Terms Translation
define('NAV_TABLE_1053','Line Fee Note on Report Hist_'); // Line Fee Note on Report Hist.
define('NAV_TABLE_1100','Cost Journal Template'); // Cost Journal Template
define('NAV_TABLE_1101','Cost Journal Line'); // Cost Journal Line
define('NAV_TABLE_1102','Cost Journal Batch'); // Cost Journal Batch
define('NAV_TABLE_1103','Cost Type'); // Cost Type
define('NAV_TABLE_1104','Cost Entry'); // Cost Entry
define('NAV_TABLE_1105','Cost Register'); // Cost Register
define('NAV_TABLE_1106','Cost Allocation Source'); // Cost Allocation Source
define('NAV_TABLE_1107','Cost Allocation Target'); // Cost Allocation Target
define('NAV_TABLE_1108','Cost Accounting Setup'); // Cost Accounting Setup
define('NAV_TABLE_1109','Cost Budget Entry'); // Cost Budget Entry
define('NAV_TABLE_1110','Cost Budget Name'); // Cost Budget Name
define('NAV_TABLE_1111','Cost Budget Register'); // Cost Budget Register
define('NAV_TABLE_1112','Cost Center'); // Cost Center
define('NAV_TABLE_1113','Cost Object'); // Cost Object
define('NAV_TABLE_1114','Cost Budget Buffer'); // Cost Budget Buffer
define('NAV_TABLE_1150','Report Totals Buffer'); // Report Totals Buffer
define('NAV_TABLE_1200','Bank Export_Import Setup'); // Bank Export/Import Setup
define('NAV_TABLE_1205','Credit Transfer Register'); // Credit Transfer Register
define('NAV_TABLE_1206','Credit Transfer Entry'); // Credit Transfer Entry
define('NAV_TABLE_1207','Direct Debit Collection'); // Direct Debit Collection
define('NAV_TABLE_1208','Direct Debit Collection Entry'); // Direct Debit Collection Entry
define('NAV_TABLE_1209','Credit Trans Re-export History'); // Credit Trans Re-export History
define('NAV_TABLE_1213','Data Exchange Type'); // Data Exchange Type
define('NAV_TABLE_1214','Intermediate Data Import'); // Intermediate Data Import
define('NAV_TABLE_1220','Data Exch_'); // Data Exch.
define('NAV_TABLE_1221','Data Exch_ Field'); // Data Exch. Field
define('NAV_TABLE_1222','Data Exch_ Def'); // Data Exch. Def
define('NAV_TABLE_1223','Data Exch_ Column Def'); // Data Exch. Column Def
define('NAV_TABLE_1224','Data Exch_ Mapping'); // Data Exch. Mapping
define('NAV_TABLE_1225','Data Exch_ Field Mapping'); // Data Exch. Field Mapping
define('NAV_TABLE_1226','Payment Export Data'); // Payment Export Data
define('NAV_TABLE_1227','Data Exch_ Line Def'); // Data Exch. Line Def
define('NAV_TABLE_1228','Payment Jnl_ Export Error Text'); // Payment Jnl. Export Error Text
define('NAV_TABLE_1229','Payment Export Remittance Text'); // Payment Export Remittance Text
define('NAV_TABLE_1230','SEPA Direct Debit Mandate'); // SEPA Direct Debit Mandate
define('NAV_TABLE_1231','Positive Pay Entry'); // Positive Pay Entry
define('NAV_TABLE_1232','Positive Pay Entry Detail'); // Positive Pay Entry Detail
define('NAV_TABLE_1234','CSV Buffer'); // CSV Buffer
define('NAV_TABLE_1235','XML Buffer'); // XML Buffer
define('NAV_TABLE_1237','Transformation Rule'); // Transformation Rule
define('NAV_TABLE_1240','Positive Pay Header'); // Positive Pay Header
define('NAV_TABLE_1241','Positive Pay Detail'); // Positive Pay Detail
define('NAV_TABLE_1242','Positive Pay Footer'); // Positive Pay Footer
define('NAV_TABLE_1248','Ledger Entry Matching Buffer'); // Ledger Entry Matching Buffer
define('NAV_TABLE_1249','Bank Stmt Multiple Match Line'); // Bank Stmt Multiple Match Line
define('NAV_TABLE_1250','Bank Statement Matching Buffer'); // Bank Statement Matching Buffer
define('NAV_TABLE_1251','Text-to-Account Mapping'); // Text-to-Account Mapping
define('NAV_TABLE_1252','Bank Pmt_ Appl_ Rule'); // Bank Pmt. Appl. Rule
define('NAV_TABLE_1259','Bank Data Conv_ Bank'); // Bank Data Conv. Bank
define('NAV_TABLE_1260','Bank Data Conv_ Service Setup'); // Bank Data Conv. Service Setup
define('NAV_TABLE_1261','Service Password'); // Service Password
define('NAV_TABLE_1265','Data Exch_ Field Mapping Buf_'); // Data Exch. Field Mapping Buf.
define('NAV_TABLE_1270','OCR Service Setup'); // OCR Service Setup
define('NAV_TABLE_1271','OCR Service Document Template'); // OCR Service Document Template
define('NAV_TABLE_1275','Doc_ Exch_ Service Setup'); // Doc. Exch. Service Setup
define('NAV_TABLE_1280','Bank Clearing Standard'); // Bank Clearing Standard
define('NAV_TABLE_1281','Bank Data Conversion Pmt_ Type'); // Bank Data Conversion Pmt. Type
define('NAV_TABLE_1293','Payment Application Proposal'); // Payment Application Proposal
define('NAV_TABLE_1294','Applied Payment Entry'); // Applied Payment Entry
define('NAV_TABLE_1295','Posted Payment Recon_ Hdr'); // Posted Payment Recon. Hdr
define('NAV_TABLE_1296','Posted Payment Recon_ Line'); // Posted Payment Recon. Line
define('NAV_TABLE_1299','Payment Matching Details'); // Payment Matching Details
define('NAV_TABLE_1300','Mini Customer Template'); // Mini Customer Template
define('NAV_TABLE_1301','Mini Item Template'); // Mini Item Template
define('NAV_TABLE_1302','Mini Dimensions Template'); // Mini Dimensions Template
define('NAV_TABLE_1303','Mini Vendor Template'); // Mini Vendor Template
define('NAV_TABLE_1304','Sales Price and Line Disc Buff'); // Sales Price and Line Disc Buff
define('NAV_TABLE_1305','Mini Pages Mapping'); // Mini Pages Mapping
define('NAV_TABLE_1306','Mini User Removed Instructions'); // Mini User Removed Instructions
define('NAV_TABLE_1310','Mini Chart Definition'); // Mini Chart Definition
define('NAV_TABLE_1311','Mini Last Used Chart'); // Mini Last Used Chart
define('NAV_TABLE_1312','Mini Trial Balance Setup'); // Mini Trial Balance Setup
define('NAV_TABLE_1313','Mini Activities Cue'); // Mini Activities Cue
define('NAV_TABLE_1319','Sales by Cust_ Grp_Chart Setup'); // Sales by Cust. Grp.Chart Setup
define('NAV_TABLE_1400','Service Connection'); // Service Connection
define('NAV_TABLE_1500','Workflow Buffer'); // Workflow Buffer
define('NAV_TABLE_1501','Workflow'); // Workflow
define('NAV_TABLE_1502','Workflow Step'); // Workflow Step
define('NAV_TABLE_1504','Workflow Step Instance'); // Workflow Step Instance
define('NAV_TABLE_1505','Workflow - Table Relation'); // Workflow - Table Relation
define('NAV_TABLE_1506','Workflow Table Relation Value'); // Workflow Table Relation Value
define('NAV_TABLE_1507','Workflow Step Buffer'); // Workflow Step Buffer
define('NAV_TABLE_1508','Workflow Category'); // Workflow Category
define('NAV_TABLE_1509','WF Event_Response Combination'); // WF Event/Response Combination
define('NAV_TABLE_1510','Notification Template'); // Notification Template
define('NAV_TABLE_1511','Notification Entry'); // Notification Entry
define('NAV_TABLE_1512','Notification Setup'); // Notification Setup
define('NAV_TABLE_1513','Notification Schedule'); // Notification Schedule
define('NAV_TABLE_1514','Sent Notification Entry'); // Sent Notification Entry
define('NAV_TABLE_1515','Dynamic Request Page Entity'); // Dynamic Request Page Entity
define('NAV_TABLE_1516','Dynamic Request Page Field'); // Dynamic Request Page Field
define('NAV_TABLE_1520','Workflow Event'); // Workflow Event
define('NAV_TABLE_1521','Workflow Response'); // Workflow Response
define('NAV_TABLE_1522','Workflow Event Queue'); // Workflow Event Queue
define('NAV_TABLE_1523','Workflow Step Argument'); // Workflow Step Argument
define('NAV_TABLE_1524','Workflow Rule'); // Workflow Rule
define('NAV_TABLE_1525','Workflow - Record Change'); // Workflow - Record Change
define('NAV_TABLE_1526','Workflow Record Change Archive'); // Workflow Record Change Archive
define('NAV_TABLE_1530','Workflow Step Instance Archive'); // Workflow Step Instance Archive
define('NAV_TABLE_1540','Workflow User Group'); // Workflow User Group
define('NAV_TABLE_1541','Workflow User Group Member'); // Workflow User Group Member
define('NAV_TABLE_1550','Restricted Record'); // Restricted Record
define('NAV_TABLE_1650','Curr_ Exch_ Rate Update Setup'); // Curr. Exch. Rate Update Setup
define('NAV_TABLE_1700','Deferral Template'); // Deferral Template
define('NAV_TABLE_1701','Deferral Header'); // Deferral Header
define('NAV_TABLE_1702','Deferral Line'); // Deferral Line
define('NAV_TABLE_1703','Deferral Post_ Buffer'); // Deferral Post. Buffer
define('NAV_TABLE_1704','Posted Deferral Header'); // Posted Deferral Header
define('NAV_TABLE_1705','Posted Deferral Line'); // Posted Deferral Line
define('NAV_TABLE_5050','Contact'); // Contact
define('NAV_TABLE_5051','Contact Alt_ Address'); // Contact Alt. Address
define('NAV_TABLE_5052','Contact Alt_ Addr_ Date Range'); // Contact Alt. Addr. Date Range
define('NAV_TABLE_5053','Business Relation'); // Business Relation
define('NAV_TABLE_5054','Contact Business Relation'); // Contact Business Relation
define('NAV_TABLE_5055','Mailing Group'); // Mailing Group
define('NAV_TABLE_5056','Contact Mailing Group'); // Contact Mailing Group
define('NAV_TABLE_5057','Industry Group'); // Industry Group
define('NAV_TABLE_5058','Contact Industry Group'); // Contact Industry Group
define('NAV_TABLE_5059','Web Source'); // Web Source
define('NAV_TABLE_5060','Contact Web Source'); // Contact Web Source
define('NAV_TABLE_5061','Rlshp_ Mgt_ Comment Line'); // Rlshp. Mgt. Comment Line
define('NAV_TABLE_5062','Attachment'); // Attachment
define('NAV_TABLE_5063','Interaction Group'); // Interaction Group
define('NAV_TABLE_5064','Interaction Template'); // Interaction Template
define('NAV_TABLE_5065','Interaction Log Entry'); // Interaction Log Entry
define('NAV_TABLE_5066','Job Responsibility'); // Job Responsibility
define('NAV_TABLE_5067','Contact Job Responsibility'); // Contact Job Responsibility
define('NAV_TABLE_5068','Salutation'); // Salutation
define('NAV_TABLE_5069','Salutation Formula'); // Salutation Formula
define('NAV_TABLE_5070','Organizational Level'); // Organizational Level
define('NAV_TABLE_5071','Campaign'); // Campaign
define('NAV_TABLE_5072','Campaign Entry'); // Campaign Entry
define('NAV_TABLE_5073','Campaign Status'); // Campaign Status
define('NAV_TABLE_5074','Delivery Sorter'); // Delivery Sorter
define('NAV_TABLE_5075','Logged Segment'); // Logged Segment
define('NAV_TABLE_5076','Segment Header'); // Segment Header
define('NAV_TABLE_5077','Segment Line'); // Segment Line
define('NAV_TABLE_5078','Segment History'); // Segment History
define('NAV_TABLE_5079','Marketing Setup'); // Marketing Setup
define('NAV_TABLE_5080','To-do'); // To-do
define('NAV_TABLE_5081','Activity'); // Activity
define('NAV_TABLE_5082','Activity Step'); // Activity Step
define('NAV_TABLE_5083','Team'); // Team
define('NAV_TABLE_5084','Team Salesperson'); // Team Salesperson
define('NAV_TABLE_5085','Contact Duplicate'); // Contact Duplicate
define('NAV_TABLE_5086','Cont_ Duplicate Search String'); // Cont. Duplicate Search String
define('NAV_TABLE_5087','Profile Questionnaire Header'); // Profile Questionnaire Header
define('NAV_TABLE_5088','Profile Questionnaire Line'); // Profile Questionnaire Line
define('NAV_TABLE_5089','Contact Profile Answer'); // Contact Profile Answer
define('NAV_TABLE_5090','Sales Cycle'); // Sales Cycle
define('NAV_TABLE_5091','Sales Cycle Stage'); // Sales Cycle Stage
define('NAV_TABLE_5092','Opportunity'); // Opportunity
define('NAV_TABLE_5093','Opportunity Entry'); // Opportunity Entry
define('NAV_TABLE_5094','Close Opportunity Code'); // Close Opportunity Code
define('NAV_TABLE_5095','Duplicate Search String Setup'); // Duplicate Search String Setup
define('NAV_TABLE_5096','Segment Wizard Filter'); // Segment Wizard Filter
define('NAV_TABLE_5097','Segment Criteria Line'); // Segment Criteria Line
define('NAV_TABLE_5098','Saved Segment Criteria'); // Saved Segment Criteria
define('NAV_TABLE_5099','Saved Segment Criteria Line'); // Saved Segment Criteria Line
define('NAV_TABLE_5100','Communication Method'); // Communication Method
define('NAV_TABLE_5101','Contact Value'); // Contact Value
define('NAV_TABLE_5102','RM Matrix Management'); // RM Matrix Management
define('NAV_TABLE_5103','Interaction Tmpl_ Language'); // Interaction Tmpl. Language
define('NAV_TABLE_5104','Segment Interaction Language'); // Segment Interaction Language
define('NAV_TABLE_5105','Customer Template'); // Customer Template
define('NAV_TABLE_5107','Sales Header Archive'); // Sales Header Archive
define('NAV_TABLE_5108','Sales Line Archive'); // Sales Line Archive
define('NAV_TABLE_5109','Purchase Header Archive'); // Purchase Header Archive
define('NAV_TABLE_5110','Purchase Line Archive'); // Purchase Line Archive
define('NAV_TABLE_5111','Rating'); // Rating
define('NAV_TABLE_5122','Interaction Template Setup'); // Interaction Template Setup
define('NAV_TABLE_5123','Inter_ Log Entry Comment Line'); // Inter. Log Entry Comment Line
define('NAV_TABLE_5124','Current Salesperson'); // Current Salesperson
define('NAV_TABLE_5125','Purch_ Comment Line Archive'); // Purch. Comment Line Archive
define('NAV_TABLE_5126','Sales Comment Line Archive'); // Sales Comment Line Archive
define('NAV_TABLE_5127','Deferral Header Archive'); // Deferral Header Archive
define('NAV_TABLE_5128','Deferral Line Archive'); // Deferral Line Archive
define('NAV_TABLE_5150','Integration Page'); // Integration Page
define('NAV_TABLE_5151','Integration Record'); // Integration Record
define('NAV_TABLE_5196','To-do Interaction Language'); // To-do Interaction Language
define('NAV_TABLE_5199','Attendee'); // Attendee
define('NAV_TABLE_5200','Employee'); // Employee
define('NAV_TABLE_5201','Alternative Address'); // Alternative Address
define('NAV_TABLE_5202','Qualification'); // Qualification
define('NAV_TABLE_5203','Employee Qualification'); // Employee Qualification
define('NAV_TABLE_5204','Relative'); // Relative
define('NAV_TABLE_5205','Employee Relative'); // Employee Relative
define('NAV_TABLE_5206','Cause of Absence'); // Cause of Absence
define('NAV_TABLE_5207','Employee Absence'); // Employee Absence
define('NAV_TABLE_5208','Human Resource Comment Line'); // Human Resource Comment Line
define('NAV_TABLE_5209','Union'); // Union
define('NAV_TABLE_5210','Cause of Inactivity'); // Cause of Inactivity
define('NAV_TABLE_5211','Employment Contract'); // Employment Contract
define('NAV_TABLE_5212','Employee Statistics Group'); // Employee Statistics Group
define('NAV_TABLE_5213','Misc_ Article'); // Misc. Article
define('NAV_TABLE_5214','Misc_ Article Information'); // Misc. Article Information
define('NAV_TABLE_5215','Confidential'); // Confidential
define('NAV_TABLE_5216','Confidential Information'); // Confidential Information
define('NAV_TABLE_5217','Grounds for Termination'); // Grounds for Termination
define('NAV_TABLE_5218','Human Resources Setup'); // Human Resources Setup
define('NAV_TABLE_5219','HR Confidential Comment Line'); // HR Confidential Comment Line
define('NAV_TABLE_5220','Human Resource Unit of Measure'); // Human Resource Unit of Measure
define('NAV_TABLE_5300','Outlook Synch_ Entity'); // Outlook Synch. Entity
define('NAV_TABLE_5301','Outlook Synch_ Entity Element'); // Outlook Synch. Entity Element
define('NAV_TABLE_5302','Outlook Synch_ Link'); // Outlook Synch. Link
define('NAV_TABLE_5303','Outlook Synch_ Filter'); // Outlook Synch. Filter
define('NAV_TABLE_5304','Outlook Synch_ Field'); // Outlook Synch. Field
define('NAV_TABLE_5305','Outlook Synch_ User Setup'); // Outlook Synch. User Setup
define('NAV_TABLE_5306','Outlook Synch_ Lookup Name'); // Outlook Synch. Lookup Name
define('NAV_TABLE_5307','Outlook Synch_ Option Correl_'); // Outlook Synch. Option Correl.
define('NAV_TABLE_5310','Outlook Synch_ Setup Detail'); // Outlook Synch. Setup Detail
define('NAV_TABLE_5311','Outlook Synch_ Dependency'); // Outlook Synch. Dependency
define('NAV_TABLE_5320','Exchange Folder'); // Exchange Folder
define('NAV_TABLE_5329','CRM Redirect'); // CRM Redirect
define('NAV_TABLE_5330','CRM Connection Setup'); // CRM Connection Setup
define('NAV_TABLE_5331','CRM Integration Record'); // CRM Integration Record
define('NAV_TABLE_5335','Integration Table Mapping'); // Integration Table Mapping
define('NAV_TABLE_5336','Integration Field Mapping'); // Integration Field Mapping
define('NAV_TABLE_5337','Temp Integration Field Mapping'); // Temp Integration Field Mapping
define('NAV_TABLE_5338','Integration Synch_ Job'); // Integration Synch. Job
define('NAV_TABLE_5339','Integration Synch_ Job Errors'); // Integration Synch. Job Errors
define('NAV_TABLE_5340','CRM Systemuser'); // CRM Systemuser
define('NAV_TABLE_5341','CRM Account'); // CRM Account
define('NAV_TABLE_5342','CRM Contact'); // CRM Contact
define('NAV_TABLE_5343','CRM Opportunity'); // CRM Opportunity
define('NAV_TABLE_5344','CRM Post'); // CRM Post
define('NAV_TABLE_5345','CRM Transactioncurrency'); // CRM Transactioncurrency
define('NAV_TABLE_5346','CRM Pricelevel'); // CRM Pricelevel
define('NAV_TABLE_5347','CRM Productpricelevel'); // CRM Productpricelevel
define('NAV_TABLE_5348','CRM Product'); // CRM Product
define('NAV_TABLE_5349','CRM Incident'); // CRM Incident
define('NAV_TABLE_5350','CRM Incidentresolution'); // CRM Incidentresolution
define('NAV_TABLE_5351','CRM Quote'); // CRM Quote
define('NAV_TABLE_5352','CRM Quotedetail'); // CRM Quotedetail
define('NAV_TABLE_5353','CRM Salesorder'); // CRM Salesorder
define('NAV_TABLE_5354','CRM Salesorderdetail'); // CRM Salesorderdetail
define('NAV_TABLE_5355','CRM Invoice'); // CRM Invoice
define('NAV_TABLE_5356','CRM Invoicedetail'); // CRM Invoicedetail
define('NAV_TABLE_5357','CRM Contract'); // CRM Contract
define('NAV_TABLE_5359','CRM Team'); // CRM Team
define('NAV_TABLE_5360','CRM Customeraddress'); // CRM Customeraddress
define('NAV_TABLE_5361','CRM Uom'); // CRM Uom
define('NAV_TABLE_5362','CRM Uomschedule'); // CRM Uomschedule
define('NAV_TABLE_5363','CRM Organization'); // CRM Organization
define('NAV_TABLE_5364','CRM Businessunit'); // CRM Businessunit
define('NAV_TABLE_5365','CRM Discount'); // CRM Discount
define('NAV_TABLE_5366','CRM Discounttype'); // CRM Discounttype
define('NAV_TABLE_5367','CRM Account Statistics'); // CRM Account Statistics
define('NAV_TABLE_5368','CRM NAV Connection'); // CRM NAV Connection
define('NAV_TABLE_5370','CRM Synch_ Job Status Cue'); // CRM Synch. Job Status Cue
define('NAV_TABLE_5371','Service Connection Error'); // Service Connection Error
define('NAV_TABLE_5372','Service Connection Status'); // Service Connection Status
define('NAV_TABLE_5401','Item Variant'); // Item Variant
define('NAV_TABLE_5402','Unit of Measure Translation'); // Unit of Measure Translation
define('NAV_TABLE_5404','Item Unit of Measure'); // Item Unit of Measure
define('NAV_TABLE_5405','Production Order'); // Production Order
define('NAV_TABLE_5406','Prod_ Order Line'); // Prod. Order Line
define('NAV_TABLE_5407','Prod_ Order Component'); // Prod. Order Component
define('NAV_TABLE_5409','Prod_ Order Routing Line'); // Prod. Order Routing Line
define('NAV_TABLE_5410','Prod_ Order Capacity Need'); // Prod. Order Capacity Need
define('NAV_TABLE_5411','Prod_ Order Routing Tool'); // Prod. Order Routing Tool
define('NAV_TABLE_5412','Prod_ Order Routing Personnel'); // Prod. Order Routing Personnel
define('NAV_TABLE_5413','Prod_ Order Rtng Qlty Meas_'); // Prod. Order Rtng Qlty Meas.
define('NAV_TABLE_5414','Prod_ Order Comment Line'); // Prod. Order Comment Line
define('NAV_TABLE_5415','Prod_ Order Rtng Comment Line'); // Prod. Order Rtng Comment Line
define('NAV_TABLE_5416','Prod_ Order Comp_ Cmt Line'); // Prod. Order Comp. Cmt Line
define('NAV_TABLE_5430','Planning Error Log'); // Planning Error Log
define('NAV_TABLE_5520','Unplanned Demand'); // Unplanned Demand
define('NAV_TABLE_5525','Manufacturing User Template'); // Manufacturing User Template
define('NAV_TABLE_5530','Inventory Event Buffer'); // Inventory Event Buffer
define('NAV_TABLE_5531','Inventory Page Data'); // Inventory Page Data
define('NAV_TABLE_5540','Timeline Event'); // Timeline Event
define('NAV_TABLE_5541','Timeline Event Change'); // Timeline Event Change
define('NAV_TABLE_5600','Fixed Asset'); // Fixed Asset
define('NAV_TABLE_5601','FA Ledger Entry'); // FA Ledger Entry
define('NAV_TABLE_5603','FA Setup'); // FA Setup
define('NAV_TABLE_5604','FA Posting Type Setup'); // FA Posting Type Setup
define('NAV_TABLE_5605','FA Journal Setup'); // FA Journal Setup
define('NAV_TABLE_5606','FA Posting Group'); // FA Posting Group
define('NAV_TABLE_5607','FA Class'); // FA Class
define('NAV_TABLE_5608','FA Subclass'); // FA Subclass
define('NAV_TABLE_5609','FA Location'); // FA Location
define('NAV_TABLE_5611','Depreciation Book'); // Depreciation Book
define('NAV_TABLE_5612','FA Depreciation Book'); // FA Depreciation Book
define('NAV_TABLE_5615','FA Allocation'); // FA Allocation
define('NAV_TABLE_5616','Maintenance Registration'); // Maintenance Registration
define('NAV_TABLE_5617','FA Register'); // FA Register
define('NAV_TABLE_5619','FA Journal Template'); // FA Journal Template
define('NAV_TABLE_5620','FA Journal Batch'); // FA Journal Batch
define('NAV_TABLE_5621','FA Journal Line'); // FA Journal Line
define('NAV_TABLE_5622','FA Reclass_ Journal Template'); // FA Reclass. Journal Template
define('NAV_TABLE_5623','FA Reclass_ Journal Batch'); // FA Reclass. Journal Batch
define('NAV_TABLE_5624','FA Reclass_ Journal Line'); // FA Reclass. Journal Line
define('NAV_TABLE_5625','Maintenance Ledger Entry'); // Maintenance Ledger Entry
define('NAV_TABLE_5626','Maintenance'); // Maintenance
define('NAV_TABLE_5628','Insurance'); // Insurance
define('NAV_TABLE_5629','Ins_ Coverage Ledger Entry'); // Ins. Coverage Ledger Entry
define('NAV_TABLE_5630','Insurance Type'); // Insurance Type
define('NAV_TABLE_5633','Insurance Journal Template'); // Insurance Journal Template
define('NAV_TABLE_5634','Insurance Journal Batch'); // Insurance Journal Batch
define('NAV_TABLE_5635','Insurance Journal Line'); // Insurance Journal Line
define('NAV_TABLE_5636','Insurance Register'); // Insurance Register
define('NAV_TABLE_5637','FA G_L Posting Buffer'); // FA G/L Posting Buffer
define('NAV_TABLE_5640','Main Asset Component'); // Main Asset Component
define('NAV_TABLE_5641','FA Buffer Projection'); // FA Buffer Projection
define('NAV_TABLE_5642','Depreciation Table Header'); // Depreciation Table Header
define('NAV_TABLE_5643','Depreciation Table Line'); // Depreciation Table Line
define('NAV_TABLE_5644','FA Posting Type'); // FA Posting Type
define('NAV_TABLE_5645','FA Date Type'); // FA Date Type
define('NAV_TABLE_5646','Depreciation Table Buffer'); // Depreciation Table Buffer
define('NAV_TABLE_5647','FA Matrix Posting Type'); // FA Matrix Posting Type
define('NAV_TABLE_5649','FA Posting Group Buffer'); // FA Posting Group Buffer
define('NAV_TABLE_5650','Total Value Insured'); // Total Value Insured
define('NAV_TABLE_5700','Stockkeeping Unit'); // Stockkeeping Unit
define('NAV_TABLE_5701','Stockkeeping Unit Comment Line'); // Stockkeeping Unit Comment Line
define('NAV_TABLE_5714','Responsibility Center'); // Responsibility Center
define('NAV_TABLE_5715','Item Substitution'); // Item Substitution
define('NAV_TABLE_5716','Substitution Condition'); // Substitution Condition
define('NAV_TABLE_5717','Item Cross Reference'); // Item Cross Reference
define('NAV_TABLE_5718','Nonstock Item'); // Nonstock Item
define('NAV_TABLE_5719','Nonstock Item Setup'); // Nonstock Item Setup
define('NAV_TABLE_5720','Manufacturer'); // Manufacturer
define('NAV_TABLE_5721','Purchasing'); // Purchasing
define('NAV_TABLE_5722','Item Category'); // Item Category
define('NAV_TABLE_5723','Product Group'); // Product Group
define('NAV_TABLE_5740','Transfer Header'); // Transfer Header
define('NAV_TABLE_5741','Transfer Line'); // Transfer Line
define('NAV_TABLE_5742','Transfer Route'); // Transfer Route
define('NAV_TABLE_5744','Transfer Shipment Header'); // Transfer Shipment Header
define('NAV_TABLE_5745','Transfer Shipment Line'); // Transfer Shipment Line
define('NAV_TABLE_5746','Transfer Receipt Header'); // Transfer Receipt Header
define('NAV_TABLE_5747','Transfer Receipt Line'); // Transfer Receipt Line
define('NAV_TABLE_5748','Inventory Comment Line'); // Inventory Comment Line
define('NAV_TABLE_5765','Warehouse Request'); // Warehouse Request
define('NAV_TABLE_5766','Warehouse Activity Header'); // Warehouse Activity Header
define('NAV_TABLE_5767','Warehouse Activity Line'); // Warehouse Activity Line
define('NAV_TABLE_5768','Whse_ Cross-Dock Opportunity'); // Whse. Cross-Dock Opportunity
define('NAV_TABLE_5769','Warehouse Setup'); // Warehouse Setup
define('NAV_TABLE_5770','Warehouse Comment Line'); // Warehouse Comment Line
define('NAV_TABLE_5771','Warehouse Source Filter'); // Warehouse Source Filter
define('NAV_TABLE_5772','Registered Whse_ Activity Hdr_'); // Registered Whse. Activity Hdr.
define('NAV_TABLE_5773','Registered Whse_ Activity Line'); // Registered Whse. Activity Line
define('NAV_TABLE_5790','Shipping Agent Services'); // Shipping Agent Services
define('NAV_TABLE_5800','Item Charge'); // Item Charge
define('NAV_TABLE_5802','Value Entry'); // Value Entry
define('NAV_TABLE_5803','Item Journal Buffer'); // Item Journal Buffer
define('NAV_TABLE_5804','Avg_ Cost Adjmt_ Entry Point'); // Avg. Cost Adjmt. Entry Point
define('NAV_TABLE_5805','Item Charge Assignment (Purch)'); // Item Charge Assignment (Purch)
define('NAV_TABLE_5809','Item Charge Assignment (Sales)'); // Item Charge Assignment (Sales)
define('NAV_TABLE_5810','Rounding Residual Buffer'); // Rounding Residual Buffer
define('NAV_TABLE_5811','Post Value Entry to G_L'); // Post Value Entry to G/L
define('NAV_TABLE_5813','Inventory Posting Setup'); // Inventory Posting Setup
define('NAV_TABLE_5814','Inventory Period'); // Inventory Period
define('NAV_TABLE_5815','Inventory Period Entry'); // Inventory Period Entry
define('NAV_TABLE_5820','Cost Element Buffer'); // Cost Element Buffer
define('NAV_TABLE_5821','Item Statistics Buffer'); // Item Statistics Buffer
define('NAV_TABLE_5822','Invt_ Post to G_L Test Buffer'); // Invt. Post to G/L Test Buffer
define('NAV_TABLE_5823','G_L - Item Ledger Relation'); // G/L - Item Ledger Relation
define('NAV_TABLE_5830','Availability Calc_ Overview'); // Availability Calc. Overview
define('NAV_TABLE_5832','Capacity Ledger Entry'); // Capacity Ledger Entry
define('NAV_TABLE_5840','Standard Cost Worksheet Name'); // Standard Cost Worksheet Name
define('NAV_TABLE_5841','Standard Cost Worksheet'); // Standard Cost Worksheet
define('NAV_TABLE_5845','Inventory Report Header'); // Inventory Report Header
define('NAV_TABLE_5846','Inventory Report Entry'); // Inventory Report Entry
define('NAV_TABLE_5847','Average Cost Calc_ Overview'); // Average Cost Calc. Overview
define('NAV_TABLE_5848','Cost Share Buffer'); // Cost Share Buffer
define('NAV_TABLE_5870','BOM Buffer'); // BOM Buffer
define('NAV_TABLE_5871','Memoized Result'); // Memoized Result
define('NAV_TABLE_5872','Item Availability by Date'); // Item Availability by Date
define('NAV_TABLE_5874','BOM Warning Log'); // BOM Warning Log
define('NAV_TABLE_5890','Error Buffer'); // Error Buffer
define('NAV_TABLE_5895','Inventory Adjustment Buffer'); // Inventory Adjustment Buffer
define('NAV_TABLE_5896','Inventory Adjmt_ Entry (Order)'); // Inventory Adjmt. Entry (Order)
define('NAV_TABLE_5900','Service Header'); // Service Header
define('NAV_TABLE_5901','Service Item Line'); // Service Item Line
define('NAV_TABLE_5902','Service Line'); // Service Line
define('NAV_TABLE_5903','Service Order Type'); // Service Order Type
define('NAV_TABLE_5904','Service Item Group'); // Service Item Group
define('NAV_TABLE_5905','Service Cost'); // Service Cost
define('NAV_TABLE_5906','Service Comment Line'); // Service Comment Line
define('NAV_TABLE_5907','Service Ledger Entry'); // Service Ledger Entry
define('NAV_TABLE_5908','Warranty Ledger Entry'); // Warranty Ledger Entry
define('NAV_TABLE_5909','Service Shipment Buffer'); // Service Shipment Buffer
define('NAV_TABLE_5910','Service Hour'); // Service Hour
define('NAV_TABLE_5911','Service Mgt_ Setup'); // Service Mgt. Setup
define('NAV_TABLE_5912','Service Document Log'); // Service Document Log
define('NAV_TABLE_5913','Loaner'); // Loaner
define('NAV_TABLE_5914','Loaner Entry'); // Loaner Entry
define('NAV_TABLE_5915','Fault Area'); // Fault Area
define('NAV_TABLE_5916','Symptom Code'); // Symptom Code
define('NAV_TABLE_5917','Fault Reason Code'); // Fault Reason Code
define('NAV_TABLE_5918','Fault Code'); // Fault Code
define('NAV_TABLE_5919','Resolution Code'); // Resolution Code
define('NAV_TABLE_5920','Fault_Resol_ Cod_ Relationship'); // Fault/Resol. Cod. Relationship
define('NAV_TABLE_5921','Fault Area_Symptom Code'); // Fault Area/Symptom Code
define('NAV_TABLE_5927','Repair Status'); // Repair Status
define('NAV_TABLE_5928','Service Status Priority Setup'); // Service Status Priority Setup
define('NAV_TABLE_5929','Service Shelf'); // Service Shelf
define('NAV_TABLE_5933','Service Order Posting Buffer'); // Service Order Posting Buffer
define('NAV_TABLE_5934','Service Register'); // Service Register
define('NAV_TABLE_5935','Service E-Mail Queue'); // Service E-Mail Queue
define('NAV_TABLE_5936','Service Document Register'); // Service Document Register
define('NAV_TABLE_5940','Service Item'); // Service Item
define('NAV_TABLE_5941','Service Item Component'); // Service Item Component
define('NAV_TABLE_5942','Service Item Log'); // Service Item Log
define('NAV_TABLE_5943','Troubleshooting Header'); // Troubleshooting Header
define('NAV_TABLE_5944','Troubleshooting Line'); // Troubleshooting Line
define('NAV_TABLE_5945','Troubleshooting Setup'); // Troubleshooting Setup
define('NAV_TABLE_5950','Service Order Allocation'); // Service Order Allocation
define('NAV_TABLE_5952','Resource Location'); // Resource Location
define('NAV_TABLE_5954','Work-Hour Template'); // Work-Hour Template
define('NAV_TABLE_5955','Skill Code'); // Skill Code
define('NAV_TABLE_5956','Resource Skill'); // Resource Skill
define('NAV_TABLE_5957','Service Zone'); // Service Zone
define('NAV_TABLE_5958','Resource Service Zone'); // Resource Service Zone
define('NAV_TABLE_5964','Service Contract Line'); // Service Contract Line
define('NAV_TABLE_5965','Service Contract Header'); // Service Contract Header
define('NAV_TABLE_5966','Contract Group'); // Contract Group
define('NAV_TABLE_5967','Contract Change Log'); // Contract Change Log
define('NAV_TABLE_5968','Service Contract Template'); // Service Contract Template
define('NAV_TABLE_5969','Contract Gain_Loss Entry'); // Contract Gain/Loss Entry
define('NAV_TABLE_5970','Filed Service Contract Header'); // Filed Service Contract Header
define('NAV_TABLE_5971','Filed Contract Line'); // Filed Contract Line
define('NAV_TABLE_5972','Contract_Service Discount'); // Contract/Service Discount
define('NAV_TABLE_5973','Service Contract Account Group'); // Service Contract Account Group
define('NAV_TABLE_5989','Service Shipment Item Line'); // Service Shipment Item Line
define('NAV_TABLE_5990','Service Shipment Header'); // Service Shipment Header
define('NAV_TABLE_5991','Service Shipment Line'); // Service Shipment Line
define('NAV_TABLE_5992','Service Invoice Header'); // Service Invoice Header
define('NAV_TABLE_5993','Service Invoice Line'); // Service Invoice Line
define('NAV_TABLE_5994','Service Cr_Memo Header'); // Service Cr.Memo Header
define('NAV_TABLE_5995','Service Cr_Memo Line'); // Service Cr.Memo Line
define('NAV_TABLE_5996','Standard Service Code'); // Standard Service Code
define('NAV_TABLE_5997','Standard Service Line'); // Standard Service Line
define('NAV_TABLE_5998','Standard Service Item Gr_ Code'); // Standard Service Item Gr. Code
define('NAV_TABLE_6080','Service Price Group'); // Service Price Group
define('NAV_TABLE_6081','Serv_ Price Group Setup'); // Serv. Price Group Setup
define('NAV_TABLE_6082','Service Price Adjustment Group'); // Service Price Adjustment Group
define('NAV_TABLE_6083','Serv_ Price Adjustment Detail'); // Serv. Price Adjustment Detail
define('NAV_TABLE_6084','Service Line Price Adjmt_'); // Service Line Price Adjmt.
define('NAV_TABLE_6502','Item Tracking Code'); // Item Tracking Code
define('NAV_TABLE_6504','Serial No_ Information'); // Serial No. Information
define('NAV_TABLE_6505','Lot No_ Information'); // Lot No. Information
define('NAV_TABLE_6506','Item Tracking Comment'); // Item Tracking Comment
define('NAV_TABLE_6507','Item Entry Relation'); // Item Entry Relation
define('NAV_TABLE_6508','Value Entry Relation'); // Value Entry Relation
define('NAV_TABLE_6509','Whse_ Item Entry Relation'); // Whse. Item Entry Relation
define('NAV_TABLE_6520','Item Tracing Buffer'); // Item Tracing Buffer
define('NAV_TABLE_6521','Item Tracing History Buffer'); // Item Tracing History Buffer
define('NAV_TABLE_6529','Record Buffer'); // Record Buffer
define('NAV_TABLE_6550','Whse_ Item Tracking Line'); // Whse. Item Tracking Line
define('NAV_TABLE_6635','Return Reason'); // Return Reason
define('NAV_TABLE_6650','Return Shipment Header'); // Return Shipment Header
define('NAV_TABLE_6651','Return Shipment Line'); // Return Shipment Line
define('NAV_TABLE_6660','Return Receipt Header'); // Return Receipt Header
define('NAV_TABLE_6661','Return Receipt Line'); // Return Receipt Line
define('NAV_TABLE_6670','Returns-Related Document'); // Returns-Related Document
define('NAV_TABLE_7002','Sales Price'); // Sales Price
define('NAV_TABLE_7004','Sales Line Discount'); // Sales Line Discount
define('NAV_TABLE_7012','Purchase Price'); // Purchase Price
define('NAV_TABLE_7014','Purchase Line Discount'); // Purchase Line Discount
define('NAV_TABLE_7023','Sales Price Worksheet'); // Sales Price Worksheet
define('NAV_TABLE_7030','Campaign Target Group'); // Campaign Target Group
define('NAV_TABLE_7110','Analysis Field Value'); // Analysis Field Value
define('NAV_TABLE_7111','Analysis Report Name'); // Analysis Report Name
define('NAV_TABLE_7112','Analysis Line Template'); // Analysis Line Template
define('NAV_TABLE_7113','Analysis Type'); // Analysis Type
define('NAV_TABLE_7114','Analysis Line'); // Analysis Line
define('NAV_TABLE_7116','Analysis Column Template'); // Analysis Column Template
define('NAV_TABLE_7118','Analysis Column'); // Analysis Column
define('NAV_TABLE_7132','Item Budget Name'); // Item Budget Name
define('NAV_TABLE_7134','Item Budget Entry'); // Item Budget Entry
define('NAV_TABLE_7136','Item Budget Buffer'); // Item Budget Buffer
define('NAV_TABLE_7152','Item Analysis View'); // Item Analysis View
define('NAV_TABLE_7153','Item Analysis View Filter'); // Item Analysis View Filter
define('NAV_TABLE_7154','Item Analysis View Entry'); // Item Analysis View Entry
define('NAV_TABLE_7156','Item Analysis View Budg_ Entry'); // Item Analysis View Budg. Entry
define('NAV_TABLE_7158','Analysis Dim_ Selection Buffer'); // Analysis Dim. Selection Buffer
define('NAV_TABLE_7159','Analysis Selected Dimension'); // Analysis Selected Dimension
define('NAV_TABLE_7190','Sales Shipment Buffer'); // Sales Shipment Buffer
define('NAV_TABLE_7300','Zone'); // Zone
define('NAV_TABLE_7301','Warehouse Employee'); // Warehouse Employee
define('NAV_TABLE_7302','Bin Content'); // Bin Content
define('NAV_TABLE_7303','Bin Type'); // Bin Type
define('NAV_TABLE_7304','Warehouse Class'); // Warehouse Class
define('NAV_TABLE_7305','Special Equipment'); // Special Equipment
define('NAV_TABLE_7307','Put-away Template Header'); // Put-away Template Header
define('NAV_TABLE_7308','Put-away Template Line'); // Put-away Template Line
define('NAV_TABLE_7309','Warehouse Journal Template'); // Warehouse Journal Template
define('NAV_TABLE_7310','Warehouse Journal Batch'); // Warehouse Journal Batch
define('NAV_TABLE_7311','Warehouse Journal Line'); // Warehouse Journal Line
define('NAV_TABLE_7312','Warehouse Entry'); // Warehouse Entry
define('NAV_TABLE_7313','Warehouse Register'); // Warehouse Register
define('NAV_TABLE_7316','Warehouse Receipt Header'); // Warehouse Receipt Header
define('NAV_TABLE_7317','Warehouse Receipt Line'); // Warehouse Receipt Line
define('NAV_TABLE_7318','Posted Whse_ Receipt Header'); // Posted Whse. Receipt Header
define('NAV_TABLE_7319','Posted Whse_ Receipt Line'); // Posted Whse. Receipt Line
define('NAV_TABLE_7320','Warehouse Shipment Header'); // Warehouse Shipment Header
define('NAV_TABLE_7321','Warehouse Shipment Line'); // Warehouse Shipment Line
define('NAV_TABLE_7322','Posted Whse_ Shipment Header'); // Posted Whse. Shipment Header
define('NAV_TABLE_7323','Posted Whse_ Shipment Line'); // Posted Whse. Shipment Line
define('NAV_TABLE_7324','Whse_ Put-away Request'); // Whse. Put-away Request
define('NAV_TABLE_7325','Whse_ Pick Request'); // Whse. Pick Request
define('NAV_TABLE_7326','Whse_ Worksheet Line'); // Whse. Worksheet Line
define('NAV_TABLE_7327','Whse_ Worksheet Name'); // Whse. Worksheet Name
define('NAV_TABLE_7328','Whse_ Worksheet Template'); // Whse. Worksheet Template
define('NAV_TABLE_7330','Bin Content Buffer'); // Bin Content Buffer
define('NAV_TABLE_7331','Whse_ Internal Put-away Header'); // Whse. Internal Put-away Header
define('NAV_TABLE_7332','Whse_ Internal Put-away Line'); // Whse. Internal Put-away Line
define('NAV_TABLE_7333','Whse_ Internal Pick Header'); // Whse. Internal Pick Header
define('NAV_TABLE_7334','Whse_ Internal Pick Line'); // Whse. Internal Pick Line
define('NAV_TABLE_7335','Bin Template'); // Bin Template
define('NAV_TABLE_7336','Bin Creation Wksh_ Template'); // Bin Creation Wksh. Template
define('NAV_TABLE_7337','Bin Creation Wksh_ Name'); // Bin Creation Wksh. Name
define('NAV_TABLE_7338','Bin Creation Worksheet Line'); // Bin Creation Worksheet Line
define('NAV_TABLE_7340','Posted Invt_ Put-away Header'); // Posted Invt. Put-away Header
define('NAV_TABLE_7341','Posted Invt_ Put-away Line'); // Posted Invt. Put-away Line
define('NAV_TABLE_7342','Posted Invt_ Pick Header'); // Posted Invt. Pick Header
define('NAV_TABLE_7343','Posted Invt_ Pick Line'); // Posted Invt. Pick Line
define('NAV_TABLE_7344','Registered Invt_ Movement Hdr_'); // Registered Invt. Movement Hdr.
define('NAV_TABLE_7345','Registered Invt_ Movement Line'); // Registered Invt. Movement Line
define('NAV_TABLE_7346','Internal Movement Header'); // Internal Movement Header
define('NAV_TABLE_7347','Internal Movement Line'); // Internal Movement Line
define('NAV_TABLE_7350','Lot Numbers by Bin Buffer'); // Lot Numbers by Bin Buffer
define('NAV_TABLE_7354','Bin'); // Bin
define('NAV_TABLE_7360','Reservation Entry Buffer'); // Reservation Entry Buffer
define('NAV_TABLE_7380','Phys_ Invt_ Item Selection'); // Phys. Invt. Item Selection
define('NAV_TABLE_7381','Phys_ Invt_ Counting Period'); // Phys. Invt. Counting Period
define('NAV_TABLE_7600','Base Calendar'); // Base Calendar
define('NAV_TABLE_7601','Base Calendar Change'); // Base Calendar Change
define('NAV_TABLE_7602','Customized Calendar Change'); // Customized Calendar Change
define('NAV_TABLE_7603','Customized Calendar Entry'); // Customized Calendar Entry
define('NAV_TABLE_7604','Where Used Base Calendar'); // Where Used Base Calendar
define('NAV_TABLE_7700','Miniform Header'); // Miniform Header
define('NAV_TABLE_7701','Miniform Line'); // Miniform Line
define('NAV_TABLE_7702','Miniform Function Group'); // Miniform Function Group
define('NAV_TABLE_7703','Miniform Function'); // Miniform Function
define('NAV_TABLE_7704','Item Identifier'); // Item Identifier
define('NAV_TABLE_7710','ADCS User'); // ADCS User
define('NAV_TABLE_8383','Dimensions Field Map'); // Dimensions Field Map
define('NAV_TABLE_8610','Config_ Questionnaire'); // Config. Questionnaire
define('NAV_TABLE_8611','Config_ Question Area'); // Config. Question Area
define('NAV_TABLE_8612','Config_ Question'); // Config. Question
define('NAV_TABLE_8613','Config_ Package Table'); // Config. Package Table
define('NAV_TABLE_8614','Config_ Package Record'); // Config. Package Record
define('NAV_TABLE_8615','Config_ Package Data'); // Config. Package Data
define('NAV_TABLE_8616','Config_ Package Field'); // Config. Package Field
define('NAV_TABLE_8617','Config_ Package Error'); // Config. Package Error
define('NAV_TABLE_8618','Config_ Template Header'); // Config. Template Header
define('NAV_TABLE_8619','Config_ Template Line'); // Config. Template Line
define('NAV_TABLE_8621','Config_ Selection'); // Config. Selection
define('NAV_TABLE_8622','Config_ Line'); // Config. Line
define('NAV_TABLE_8623','Config_ Package'); // Config. Package
define('NAV_TABLE_8624','Config_ Related Field'); // Config. Related Field
define('NAV_TABLE_8625','Config_ Related Table'); // Config. Related Table
define('NAV_TABLE_8626','Config_ Package Filter'); // Config. Package Filter
define('NAV_TABLE_8627','Config_ Setup'); // Config. Setup
define('NAV_TABLE_8628','Config_ Field Mapping'); // Config. Field Mapping
define('NAV_TABLE_8640','Config_ Text Transformation'); // Config. Text Transformation
define('NAV_TABLE_8650','DataExch-RapidStart Buffer'); // DataExch-RapidStart Buffer
define('NAV_TABLE_9000','User Group'); // User Group
define('NAV_TABLE_9001','User Group Member'); // User Group Member
define('NAV_TABLE_9002','User Group Access Control'); // User Group Access Control
define('NAV_TABLE_9003','User Group Permission Set'); // User Group Permission Set
define('NAV_TABLE_9050','Warehouse Basic Cue'); // Warehouse Basic Cue
define('NAV_TABLE_9051','Warehouse WMS Cue'); // Warehouse WMS Cue
define('NAV_TABLE_9052','Service Cue'); // Service Cue
define('NAV_TABLE_9053','Sales Cue'); // Sales Cue
define('NAV_TABLE_9054','Finance Cue'); // Finance Cue
define('NAV_TABLE_9055','Purchase Cue'); // Purchase Cue
define('NAV_TABLE_9056','Manufacturing Cue'); // Manufacturing Cue
define('NAV_TABLE_9057','Job Cue'); // Job Cue
define('NAV_TABLE_9058','Warehouse Worker WMS Cue'); // Warehouse Worker WMS Cue
define('NAV_TABLE_9059','Administration Cue'); // Administration Cue
define('NAV_TABLE_9060','SB Owner Cue'); // SB Owner Cue
define('NAV_TABLE_9061','RapidStart Services Cue'); // RapidStart Services Cue
define('NAV_TABLE_9070','Accounting Services Cue'); // Accounting Services Cue
define('NAV_TABLE_9150','My Customer'); // My Customer
define('NAV_TABLE_9151','My Vendor'); // My Vendor
define('NAV_TABLE_9152','My Item'); // My Item
define('NAV_TABLE_9170','Profile Resource Import_Export'); // Profile Resource Import/Export
define('NAV_TABLE_9180','Generic Chart Setup'); // Generic Chart Setup
define('NAV_TABLE_9181','Generic Chart Filter'); // Generic Chart Filter
define('NAV_TABLE_9182','Generic Chart Y-Axis'); // Generic Chart Y-Axis
define('NAV_TABLE_9183','Generic Chart Query Column'); // Generic Chart Query Column
define('NAV_TABLE_9185','Generic Chart Captions Buffer'); // Generic Chart Captions Buffer
define('NAV_TABLE_9186','Generic Chart Memo Buffer'); // Generic Chart Memo Buffer
define('NAV_TABLE_9500','Email Item'); // Email Item
define('NAV_TABLE_9600','XML Schema'); // XML Schema
define('NAV_TABLE_9610','XML Schema Element'); // XML Schema Element
define('NAV_TABLE_9611','XML Schema Restriction'); // XML Schema Restriction
define('NAV_TABLE_9612','Referenced XML Schema'); // Referenced XML Schema
define('NAV_TABLE_9650','Custom Report Layout'); // Custom Report Layout
define('NAV_TABLE_9651','Report Layout Selection'); // Report Layout Selection
define('NAV_TABLE_9656','Report Layout Update Log'); // Report Layout Update Log
define('NAV_TABLE_9657','Custom Report Selection'); // Custom Report Selection
define('NAV_TABLE_9701','Cue Setup'); // Cue Setup
define('NAV_TABLE_9800','Table Permission Buffer'); // Table Permission Buffer
define('NAV_TABLE_9805','Table Filter'); // Table Filter
define('NAV_TABLE_50000','No_ Faktur Reference'); // No. Faktur Reference
define('NAV_TABLE_50001','No_ Faktur Register'); // No. Faktur Register
define('NAV_TABLE_50002','Prepmt Inv_ Buffer_'); // Prepmt Inv. Buffer.
define('NAV_TABLE_130400','CAL Test Suite'); // CAL Test Suite
define('NAV_TABLE_130401','CAL Test Line'); // CAL Test Line
define('NAV_TABLE_130402','CAL Test Codeunit'); // CAL Test Codeunit
define('NAV_TABLE_130403','CAL Test Enabled Codeunit'); // CAL Test Enabled Codeunit
define('NAV_TABLE_130404','CAL Test Method'); // CAL Test Method
define('NAV_TABLE_130405','CAL Test Result'); // CAL Test Result
define('NAV_TABLE_130406','CAL Test Coverage Map'); // CAL Test Coverage Map
define('NAV_TABLE_99000750','Work Shift'); // Work Shift
define('NAV_TABLE_99000751','Shop Calendar'); // Shop Calendar
define('NAV_TABLE_99000752','Shop Calendar Working Days'); // Shop Calendar Working Days
define('NAV_TABLE_99000753','Shop Calendar Holiday'); // Shop Calendar Holiday
define('NAV_TABLE_99000754','Work Center'); // Work Center
define('NAV_TABLE_99000756','Work Center Group'); // Work Center Group
define('NAV_TABLE_99000757','Calendar Entry'); // Calendar Entry
define('NAV_TABLE_99000758','Machine Center'); // Machine Center
define('NAV_TABLE_99000760','Calendar Absence Entry'); // Calendar Absence Entry
define('NAV_TABLE_99000761','Stop'); // Stop
define('NAV_TABLE_99000762','Scrap'); // Scrap
define('NAV_TABLE_99000763','Routing Header'); // Routing Header
define('NAV_TABLE_99000764','Routing Line'); // Routing Line
define('NAV_TABLE_99000765','Manufacturing Setup'); // Manufacturing Setup
define('NAV_TABLE_99000770','Manufacturing Comment Line'); // Manufacturing Comment Line
define('NAV_TABLE_99000771','Production BOM Header'); // Production BOM Header
define('NAV_TABLE_99000772','Production BOM Line'); // Production BOM Line
define('NAV_TABLE_99000773','Family'); // Family
define('NAV_TABLE_99000774','Family Line'); // Family Line
define('NAV_TABLE_99000775','Routing Comment Line'); // Routing Comment Line
define('NAV_TABLE_99000776','Production BOM Comment Line'); // Production BOM Comment Line
define('NAV_TABLE_99000777','Routing Link'); // Routing Link
define('NAV_TABLE_99000778','Standard Task'); // Standard Task
define('NAV_TABLE_99000779','Production BOM Version'); // Production BOM Version
define('NAV_TABLE_99000780','Capacity Unit of Measure'); // Capacity Unit of Measure
define('NAV_TABLE_99000781','Standard Task Tool'); // Standard Task Tool
define('NAV_TABLE_99000782','Standard Task Personnel'); // Standard Task Personnel
define('NAV_TABLE_99000783','Standard Task Description'); // Standard Task Description
define('NAV_TABLE_99000784','Standard Task Quality Measure'); // Standard Task Quality Measure
define('NAV_TABLE_99000785','Quality Measure'); // Quality Measure
define('NAV_TABLE_99000786','Routing Version'); // Routing Version
define('NAV_TABLE_99000788','Production Matrix BOM Line'); // Production Matrix BOM Line
define('NAV_TABLE_99000789','Production Matrix BOM Entry'); // Production Matrix BOM Entry
define('NAV_TABLE_99000790','Where-Used Line'); // Where-Used Line
define('NAV_TABLE_99000799','Order Tracking Entry'); // Order Tracking Entry
define('NAV_TABLE_99000800','Sales Planning Line'); // Sales Planning Line
define('NAV_TABLE_99000802','Routing Tool'); // Routing Tool
define('NAV_TABLE_99000803','Routing Personnel'); // Routing Personnel
define('NAV_TABLE_99000805','Routing Quality Measure'); // Routing Quality Measure
define('NAV_TABLE_99000829','Planning Component'); // Planning Component
define('NAV_TABLE_99000830','Planning Routing Line'); // Planning Routing Line
define('NAV_TABLE_99000832','Item Availability Line'); // Item Availability Line
define('NAV_TABLE_99000846','Planning Buffer'); // Planning Buffer
define('NAV_TABLE_99000848','Registered Absence'); // Registered Absence
define('NAV_TABLE_99000849','Action Message Entry'); // Action Message Entry
define('NAV_TABLE_99000850','Planning Assignment'); // Planning Assignment
define('NAV_TABLE_99000851','Production Forecast Name'); // Production Forecast Name
define('NAV_TABLE_99000852','Production Forecast Entry'); // Production Forecast Entry
define('NAV_TABLE_99000853','Inventory Profile'); // Inventory Profile
define('NAV_TABLE_99000854','Inventory Profile Track Buffer'); // Inventory Profile Track Buffer
define('NAV_TABLE_99000855','Untracked Planning Element'); // Untracked Planning Element
define('NAV_TABLE_99000866','Capacity Constrained Resource'); // Capacity Constrained Resource
define('NAV_TABLE_99000875','Order Promising Setup'); // Order Promising Setup
define('NAV_TABLE_99000880','Order Promising Line'); // Order Promising Line
define('NAV_TABLE_99008535','TempBlob'); // TempBlob
define('NAV_TABLE_2000000004','Permission Set'); // Permission Set
define('NAV_TABLE_2000000005','Permission'); // Permission
define('NAV_TABLE_2000000006','Company'); // Company
define('NAV_TABLE_2000000053','Access Control'); // Access Control
define('NAV_TABLE_2000000065','Send-To Program'); // Send-To Program
define('NAV_TABLE_2000000066','Style Sheet'); // Style Sheet
define('NAV_TABLE_2000000067','User Default Style Sheet'); // User Default Style Sheet
define('NAV_TABLE_2000000068','Record Link'); // Record Link
define('NAV_TABLE_2000000069','Add-in'); // Add-in
define('NAV_TABLE_2000000071','Object Metadata'); // Object Metadata
define('NAV_TABLE_2000000072','Profile'); // Profile
define('NAV_TABLE_2000000073','User Personalization'); // User Personalization
define('NAV_TABLE_2000000074','Profile Metadata'); // Profile Metadata
define('NAV_TABLE_2000000075','User Metadata'); // User Metadata
define('NAV_TABLE_2000000076','Web Service'); // Web Service
define('NAV_TABLE_2000000078','Chart'); // Chart
define('NAV_TABLE_2000000079','Object Tracking'); // Object Tracking
define('NAV_TABLE_2000000080','Page Data Personalization'); // Page Data Personalization
define('NAV_TABLE_2000000100','Debugger Breakpoint'); // Debugger Breakpoint
define('NAV_TABLE_2000000104','Debugger Watch'); // Debugger Watch
define('NAV_TABLE_2000000110','Active Session'); // Active Session
define('NAV_TABLE_2000000111','Session Event'); // Session Event
define('NAV_TABLE_2000000112','Server Instance'); // Server Instance
define('NAV_TABLE_2000000114','Document Service'); // Document Service
define('NAV_TABLE_2000000120','User'); // User
define('NAV_TABLE_2000000121','User Property'); // User Property
define('NAV_TABLE_2000000130','Device'); // Device
define('NAV_TABLE_2000000150','NAV App Object Metadata'); // NAV App Object Metadata
define('NAV_TABLE_2000000151','NAV App Tenant App'); // NAV App Tenant App
define('NAV_TABLE_2000000152','NAV App Data Archive'); // NAV App Data Archive
define('NAV_TABLE_2000000153','NAV App Installed App'); // NAV App Installed App
define('NAV_TABLE_2000000160','NAV App'); // NAV App
define('NAV_TABLE_2000000161','NAV App Dependencies'); // NAV App Dependencies
define('NAV_TABLE_2000000162','NAV App Capabilities'); // NAV App Capabilities
define('NAV_TABLE_2000000163','NAV App Object Prerequisites'); // NAV App Object Prerequisites
define('NAV_TABLE_2000000165','Tenant Permission Set'); // Tenant Permission Set
define('NAV_TABLE_2000000166','Tenant Permission'); // Tenant Permission