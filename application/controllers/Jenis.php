<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Jenis extends MY_Controller {

	public function __construct()
	{
		parent ::__construct();

		$this->db2 = $this->load->database('NAV90', TRUE);
		$this->db3 = $this->load->database('pmis', TRUE);
		$this->load->model('Pmis_model');	
		$this->load->model('Jenis_model');	
		$this->is_logged_in(); 

	}

	public function index()
	{
		$this->require_min_level(1);

		$data = array(
			'title'			=>	'Jenis Aset',
			'data'			=>	$this->Jenis_model->getAll()->result(),
			'main_view'		=>	'jenis/list'
		);

		$data['stylesheet'] = array(
			"https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css",
			"https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css",
			"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css",
			"https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.bootstrap4.min.css",
			"https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
		);

		$data['javascripts'] = array(
			"https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js",
			"https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js",
			"https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js",
			"https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js",
			"https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js",
			"https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js",
			"https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js",
			"https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js",
			"https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js",
			"https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js",
			"https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"
		);

		$data['final_script'] = "
		 $(document).ready(function() {
		    $('#table').DataTable( {
		    	 dom: 'Bfrtip',
			        buttons: [
			            'copy', 'excel'
			        ],
		         scrollY:        500,
			     scrollX:        true,
			     scrollCollapse: true,
			     paging:         true,
			     fixedColumns:   {
		            leftColumns: 1
		        }
		    } );
		} );	
		";
		
		$this->breadcrumb->add('Home', site_url('pmis/'));
		$this->breadcrumb->add('Jenis', site_url('jenis/'));
		$this->load->view('themes/template', $data);

  		
	}

	public function save() {

		$this->require_min_level(1);

		$this->form_validation->set_rules('j_Name','Jenis Barang','required');

		if($this->form_validation->run() == FALSE) {

				$data = array(	

					'title'		=> 'Tambah Jenis Aset',
					'main_view'	=> 'jenis/new'
				);

				$data['stylesheet'] = array(
					base_url('assets/plugins/select2/css/select2.min.css'),
					base_url('assets/plugins/datetimepicker/css/daterangepicker.css'),
					"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"
				);

				$data['javascripts'] = array(
					"https://code.jquery.com/ui/1.12.1/jquery-ui.js",
					base_url('assets/js/sorttable.js'),
					base_url('assets/plugins/select2/js/select2.min.js'),
					base_url('assets/js/my.js'),
					base_url('assets/plugins/datetimepicker/js/moment.min.js'),
					base_url('assets/plugins/datetimepicker/js/daterangepicker.js')
				);


				$data['final_script'] = "	
				";

				$this->breadcrumb->add('Home', site_url('pmis/'));
				$this->breadcrumb->add('Jenis', site_url('jenis/'));
				$this->breadcrumb->add('Add', site_url('jenis/save'));
				$this->load->view('themes/template',$data);

		}
		else 
		{

			$i 	= $this->input;
			//Cek duplikasi data
			$file_name = $i->post('j_Name');

			$cek = $this->Jenis_model->cek_data($file_name);
			if ($cek > 0){ //jika ada data yang sama

				$this->session->set_flashdata('error', ' Jenis Barang is already exists ');
				redirect('jenis/save');

			}
			else
			{ //tidak ada data yang sama

				$data = array(

					'j_Name'		=> $file_name,
					'CreatedBy' 	=> $this->auth_username
				);

				$save = $this->Jenis_model->save('tbl_jenis', $data);
				if ($save == true){
					$this->session->set_flashdata('success','Jenis Barang has been added');
					helper_log("add", "menambahkan jenis aset '".$file_name."'", $this->auth_username);
					redirect('jenis/');
				}
				else 
				{
					$this->session->set_flashdata('error','Something is wrong!');
					redirect('jenis/save/');
				}	
			}

		}

	}

	public function edit () {
		
		$this->require_min_level(1);
		$id  = $this->uri->segment(3);
    	// cek id exist
    	$ci = $this->Jenis_model->edit($id);
    	if ($ci != false){

			$this->form_validation->set_rules('j_Name','Jenis Barang','required');

			if($this->form_validation->run() == FALSE) {

	   				$data = array(	

						'title'		=> 'Edit Jenis Aset',
						'data' 		=> 	$ci,
						'main_view'	=> 'jenis/edit'
					);

					$data['stylesheet'] = array(
						base_url('assets/plugins/select2/css/select2.min.css'),
						"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"

					);

					$data['javascripts'] = array(
						base_url('assets/plugins/select2/js/select2.min.js'),
						base_url('assets/js/my.js'),
						"https://code.jquery.com/ui/1.12.1/jquery-ui.js"
					);


					$data['final_script'] = "";

	   				$this->breadcrumb->add('Home', site_url('pmis/'));
					$this->breadcrumb->add('Jenis', site_url('jenis/'));
					$this->breadcrumb->add('Edit', site_url('jenis/edit/'));
					$this->load->view('themes/template',$data);

	   		}
	   		else 
	   		{

				$id  = $this->uri->segment(3);

				$i 	= $this->input;

				$file_name = $i->post('j_Name');

				$data = array(

					'j_Name'		=> $file_name,
					'UpdatedBy' 	=> $this->auth_username
					
				);

				$where = array('j_ID'		=> $id );

				$update = $this->Jenis_model->update('tbl_jenis', $data, $where);
				if ($update == true){
					$this->session->set_flashdata('success','Jenis Barang has been edited');
					helper_log("edit", "mengedit jenis aset '".$file_name."'", $this->auth_username);
				}
				else
				{
					$this->session->set_flashdata('info','Jenis Barang still same');
				}
				
				redirect('jenis/');

	    	}
	    }	
    	else
    	{
    		$this->session->set_flashdata('error','Jenis Barang does not exists');
    		redirect('jenis/');
    	}
		    
    }

    // Delete
	public function delete() {
		$this->require_min_level(1);
		$id = $this->uri->segment(3);
		//var_dump($id);
		$n = 1;
		$i 	= $this->input;
			
		$data  = array('discard'		=> $n,'UpdatedBy' 	=> $this->auth_username);
		$where = array('j_ID'	=> $id);

		$update = $this->Jenis_model->update('tbl_jenis', $data, $where);
			if ($update == true):
				$file_name = $this->Jenis_model->getDelete($id);
				$this->session->set_flashdata('info', ' Jenis Barang has been removed ');
				helper_log("delete", "mengahapus jenis aset '".$file_name."'", $this->auth_username);
			endif;
			redirect('jenis/');
	}
	
}