<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Pmis extends MY_Controller {

	public function __construct()
	{
		parent ::__construct();

		$this->db2 = $this->load->database('NAV90', TRUE);
		$this->db3 = $this->load->database('pmis', TRUE);
		$this->load->model('Pmis_model');
		$this->load->model('Jenis_model');
		$this->is_logged_in(); 

	}

	public function index()
	{
		$this->require_min_level(1);

		$data = array(
			'title'			=>	'Home',
			'data'			=>  $this->Pmis_model->getAll(),		
			'main_view'		=>	'aset/list'
		);

		$data['stylesheet'] = array(
			"https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css",
			"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css",

		);

		$data['custom_style'] = 
			"";

		$data['javascripts'] = array(
			"https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js",
			"https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js",
			"https://code.jquery.com/ui/1.12.1/jquery-ui.js",
			"https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.min.js",
			"https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.min.js",
			base_url('assets/js/sorttable.js')
		);

		$data['final_script'] = "";
		
		$this->breadcrumb->add('Home', site_url('pga/'));
		$this->load->view('themes/template', $data);

  		
	}	

	public function save()
    {

		$this->require_min_level(1);

		$v = $this->form_validation;
		$v->set_rules('j_ID','Jenis Aset','required');
		$v->set_rules('a_Name','Asset Name','trim|required');
		$v->set_rules('a_Entity','Entity','required');
		$v->set_rules('a_Date','Tanggal Beli Aset','required');
		$v->set_rules('u_Date','Tanggal Mulai Pemakaian','required');
		$v->set_rules('a_PIC','User','required');
		$v->set_rules('a_Jmlh','Jumlah','required');
		$v->set_rules('a_UOM','UOM','required');
		$v->set_rules('a_Location','Lokasi','required');
		$v->set_rules('a_Dept','Departemen','required');
		$v->set_rules('a_Status','Status','required');


		if($v->run() == FALSE) {

			$data = array(	

				'title'		=> 'Tambah Aset ',
				'data'		=>  $this->Jenis_model->getAll(),
				'main_view'	=> 'aset/new'
			);


			$data['stylesheet'] = array(
				"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css",
				"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"

			);

			$data['custom_style'] = 
			".select2-selection.required {
   				background-color: yellow !important;
			}";

			$data['javascripts'] = array(
				"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.full.min.js",
				"https://code.jquery.com/ui/1.12.1/jquery-ui.js",
				base_url('assets/js/my.js'),
			);


			$data['final_script'] = "
				$(document).ready(function() {
	    			$('.select2').select2();
	    			$('#date1').datepicker({
	    				changeMonth: true,
 						changeYear: true,
						'dateFormat': 'dd-M-yy',
						'showAnim': 'blind',
						'altField': '#date1-alternate',
		  				'altFormat': 'yy-mm-dd'
					});
					$('#date2').datepicker({
	    				changeMonth: true,
 						changeYear: true,
						'dateFormat': 'dd-M-yy',
						'showAnim': 'blind',
						'altField': '#date2-alternate',
		  				'altFormat': 'yy-mm-dd'
					});
				});	
			";

			$this->breadcrumb->add('Home', site_url('pmis/'));
			$this->breadcrumb->add('Add', site_url('pmis/save/'));
			$this->load->view('themes/template',$data);

		}
		else 
		{
			
			$i = $this->input;

			$file_name = $i->post('a_Name');
			$pic = $i->post('a_PIC');

			$cek = $this->Pmis_model->cek_data($file_name);

			if ($cek != 0){ //jika ada data yang sama

				$this->session->set_flashdata('error', ' Asset is already exists ');
				redirect('pmis/save');

			}
			else
			{ 

				$data = array(
					
					'j_ID'			=> $i->post('j_ID'),
					'a_Name'		=> $file_name,
					'a_Entity'		=> $i->post('a_Entity'),
					'a_Date'		=> $i->post('a_Date'),
					'a_Location'	=> $i->post('a_Location'),
					'a_Dept'		=> $i->post('a_Dept'),
					'a_Jumlah'		=> $i->post('a_Jmlh'),
					'a_UOM'			=> $i->post('a_UOM'),
					'a_Status'		=> $i->post('a_Status'),
					'a_Ket'			=> $i->post('a_Ket'),
					'a_Fixed'		=> $i->post('a_Fixed'),
					'CreatedBy'	=> $this->auth_username
			
				);
				
				$save = $this->Pmis_model->save('tbl_asset', $data);
				if ($save == true){

					$idn = $this->Pmis_model->getIDNew();

					$data = array(
					
						'a_ID'			=> $idn,
						'u_Name'		=> $pic,
						'u_Date'		=> $i->post('u_Date'),
						'CreatedBy'		=> $this->auth_username
				
					);
					$save1 = $this->Pmis_model->save('tbl_user', $data);

					if ($save1 == true){
					$this->session->set_flashdata('success','Asset has been added');
					helper_log("add", "menambahkan aset '".$file_name."'", $this->auth_username);
					redirect('pmis/');
					}
				}
				else 
				{
					$this->session->set_flashdata('error','Something is wrong!');
					redirect('pmis/save/');
				}	
			}
		}
		// End masuk database
    }

    /*------------------- EDITING PROCESS ------------------------------*/

      public function edit () {
		
		$this->require_min_level(1);
    	$id = $this->uri->segment(3);
    	// cek id exist
    	$ci = $this->Pmis_model->edit($id);
    	if ($ci != false){

    		$v = $this->form_validation;
    		$v->set_rules('j_ID','Jenis Aset','required');
			$v->set_rules('a_Name','Asset Name','trim|required');
			$v->set_rules('a_Entity','Entity','required');
			$v->set_rules('a_Date','Tanggal Beli Aset','required');
			$v->set_rules('u_Date','Tanggal Mulai Pemakaian','required');
			$v->set_rules('a_PIC','User','required');
			$v->set_rules('a_Jmlh','Jumlah','required');
			$v->set_rules('a_UOM','UOM','required');
			$v->set_rules('a_Location','Lokasi','required');
			$v->set_rules('a_Dept','Departemen','required');
			$v->set_rules('a_Status','Status','required');

			if($v->run() == FALSE) {

	   				$data = array(	

						'title'		=> 'Edit Aset ',
						'jenis'		=>  $this->Jenis_model->getAll(),
						'data' 		=> 	$ci,
						'main_view'	=> 'aset/edit'
					);

					$data['stylesheet'] = array(
						base_url('assets/plugins/select2/css/select2.min.css'),
						"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"

					);

					$data['javascripts'] = array(
						base_url('assets/plugins/select2/js/select2.min.js'),
						"https://code.jquery.com/ui/1.12.1/jquery-ui.js"
					);


					$data['final_script'] = "
					$(document).ready(function() {
	    				$('.select2').select2();
	    				$('#date1').datepicker({
		    				changeMonth: true,
	 						changeYear: true,
							'dateFormat': 'dd-M-yy',
							'showAnim': 'blind',
							'altField': '#date1-alternate',
			  				'altFormat': 'yy-mm-dd'
						});
						$('#date2').datepicker({
		    				changeMonth: true,
	 						changeYear: true,
							'dateFormat': 'dd-M-yy',
							'showAnim': 'blind',
							'altField': '#date2-alternate',
			  				'altFormat': 'yy-mm-dd'
						});
					});		
					";

	   				$this->breadcrumb->add('Home', site_url('pmis/'));
					$this->breadcrumb->add('Edit', site_url('pmis/save/'));
					$this->load->view('themes/template',$data);

	   		}
	   		else 
	   		{


				$id  = $this->uri->segment(3);				
				$i 	= $this->input;

				$file_name = $i->post('a_Name');
				$pic = $i->post('a_PIC');

				$data = array(	

					'j_ID'			=> $i->post('j_ID'),
					'a_Name'		=> $file_name,
					'a_Entity'		=> $i->post('a_Entity'),
					'a_Date'		=> $i->post('a_Date'),
					'a_Location'	=> $i->post('a_Location'),
					'a_Dept'		=> $i->post('a_Dept'),
					'a_Jumlah'		=> $i->post('a_Jmlh'),
					'a_UOM'			=> $i->post('a_UOM'),
					'a_Status'		=> $i->post('a_Status'),
					'a_Ket'			=> $i->post('a_Ket'),
					'a_Fixed'		=> $i->post('a_Fixed'),
					'UpdatedBy'		=> $this->auth_username
					
				);

				$where = array (

					'a_ID'		=> $id,

				);

				$update = $this->Jenis_model->update('tbl_asset', $data, $where);
				if ($update == true){

					$ci = $this->Pmis_model->getName2($pic, $id);
    				if ($ci == 0){

						$idn = $this->Pmis_model->getIDNew();

						$data = array(
						
							'a_ID'			=> $idn,
							'u_Name'		=> $pic,
							'u_Date'		=> $i->post('u_Date'),
							'CreatedBy'		=> $this->auth_username
					
						);
						$save1 = $this->Pmis_model->save('tbl_user', $data);
						
						if ($save1 == true)
						{
							$this->session->set_flashdata('success','Aset has been edited');
							helper_log("edit", "mengubah nama user aset '".$file_name."' menjadi '".$pic."'", $this->auth_username);
							helper_log("edit", "mengedit aset '".$file_name."'", $this->auth_username);
						}
					}
					else
					{
						$this->session->set_flashdata('success','Aset has been edited');
						helper_log("edit", "mengedit aset '".$file_name."'", $this->auth_username);
					}
					
				}
				else
				{
					$this->session->set_flashdata('info','Aset still same');
				}
				redirect('pmis/');

	    	}
	    }	
    	else
    	{
    		$this->session->set_flashdata('error','Jenis Barang does not exists');
    		redirect('pmis/');
    	}
		// End masuk database
	}

	

	/*------------------- DELETE PROCESS --------------------------*/

	// Delete
	public function delete() {
		$this->require_min_level(1);
		$id = $this->uri->segment(3);
		//var_dump($id);
		$n = 1;
		$i 	= $this->input;
			
		$data  = array('discard'		=> $n,'UpdatedBY' 	=> $this->auth_username);
		$where = array('a_ID'	=> $id);

		$update = $this->Pmis_model->update('tbl_asset', $data, $where);
			if ($update == true):
				$this->session->set_flashdata('info', ' Asset has been removed ');
				$file_name = $this->Pmis_model->getDelete($id);
				helper_log("delete", "mengahapus aset '".$file_name."'", $this->auth_username);
			endif;
			redirect('pmis/');
	}

    /*------------------- JQUERY/MODAL/SCRIPT SESSION ------------------*/

    public function log()
    {
        $this->require_min_level(3);

        $data = array(
            'title'         =>  'LOG',
            'data'          =>  $this->Pmis_model->getLog(),
            'main_view'     =>  'aset/log'
        );

        $data['stylesheet'] = array(
            "https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css",
            "https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css",
            "https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css",
            "https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.bootstrap4.min.css",
            "https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css",
            "https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.8/themes/default-dark/style.min.css",
            base_url('assets/plugins/jquery.filer/css/jquery.filer.css'),
            base_url('assets/plugins/jquery.filer/css/themes/jquery.filer-dragdropbox-theme.css'),
        );
     
        $data['javascripts'] = array(
            "https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js",
            "https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js",
            "https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js",
            "https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js",
            "https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.8/jstree.min.js",
            base_url('assets/plugins/jquery.filer/js/jquery.filer.min.js')
        );

        $data['final_script'] = "
       $(document).ready(function() {
            $('#table').DataTable( {
            });
        });
        ";
        
        $this->breadcrumb->add('Home', site_url('pmis/'));
		$this->breadcrumb->add('LOG', site_url('pmis/log/'));
        $this->load->view('themes/template', $data);

    }

}