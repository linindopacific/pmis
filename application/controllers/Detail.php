<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Detail extends MY_Controller {

	public function __construct()
	{
		parent ::__construct();

		$this->db2 = $this->load->database('NAV90', TRUE);
		$this->db3 = $this->load->database('pmis', TRUE);
		$this->load->model('Pmis_model');	
		$this->load->model('Jenis_model');
		$this->load->model('Detail_model');
		$this->is_logged_in(); 

	}

	public function index()
	{
		$this->require_min_level(3);

		$id = $this->uri->segment(3);

		$data = array(
			'title'			=>	'Detail Aset',
			'data'			=>  $this->Detail_model->getAset($id),
			'data1'			=>  $this->Detail_model->getAll($id),
			'id'			=> 	$id,		
			'main_view'		=>	'aset/list_detail'
		);

		$data['stylesheet'] = array(
			"https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css",
			"https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css",
			"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css",
			"https://cdn.datatables.net/fixedcolumns/3.2.6/css/fixedColumns.bootstrap4.min.css",
			"https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css"
		);
     
		$data['javascripts'] = array(
			"https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js",
			"https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js",
			"https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js",
			"https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js",
			"https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js",
			"https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js",
			"https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js",
			"https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js",
			"https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js",
			"https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js",
			"https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js",
			base_url('assets/js/sorttable.js')
		);

		$data['final_script'] = "
			$(document).ready(function() {
			    $('#table').DataTable( {
			    	 dom: 'Bfrtip',
				        buttons: [
				            'copy', 'excel'
				        ],
			         scrollY:        500,
				     scrollX:        true,
				     scrollCollapse: true,
				     paging:         true,
				     fixedColumns:   {
			            leftColumns: 2
			        }
			    });
			});
		";
		
		$this->breadcrumb->add('Home', site_url('pmis/'));
		$this->breadcrumb->add('Detail', site_url('detail/'.$id));
		$this->load->view('themes/template', $data);

  		
	}

	public function save() {

		$this->require_min_level(1);

		$id = $this->uri->segment(3);

		$this->form_validation->set_rules('d_Name','Computer Name','required');
		$this->form_validation->set_rules('d_Spec','Spesifikasi','required');
		$this->form_validation->set_rules('d_Fixed','No. FIxed Asset','required');
		$this->form_validation->set_rules('d_Ket','Keterangan','required');
		$this->form_validation->set_rules('d_Status','Status','required');

		if($this->form_validation->run() == FALSE) {

				$data = array(	

					'title'		=> 'Tambah Detail Aset',
					'data'		=>  $this->Detail_model->getAset($id),
					'id'		=> 	$id,
					'main_view'	=> 'aset/new_detail'
				);

				$data['stylesheet'] = array(
					"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"
				);

				$data['javascripts'] = array(
					"https://code.jquery.com/ui/1.12.1/jquery-ui.js",
					base_url('assets/js/sorttable.js'),
					base_url('assets/js/my.js'),
					base_url('assets/plugins/datetimepicker/js/moment.min.js'),
				);


				$data['final_script'] = "
					$(document).ready(function() {
		    			$('#date').datepicker({
		    				changeMonth: true,
	 						changeYear: true,
							'dateFormat': 'dd-M-yy',
							'showAnim': 'blind',
							'altField': '#date-alternate',
			  				'altFormat': 'yy-mm-dd'
						});
					});
				";

				$this->breadcrumb->add('Home', site_url('pmis/'));
				$this->breadcrumb->add('Detail', site_url('detail/index/'.$id));
				$this->breadcrumb->add('Add', site_url('detail/save/'.$id));
				$this->load->view('themes/template',$data);

		}
		else 
		{

			$i 	= $this->input;
			$id = $this->uri->segment(3);

			$file_name = $i->post('d_Name');

			$cek = $this->Detail_model->cek_data($file_name);
			if ($cek > 0){ //jika ada data yang sama

				$this->session->set_flashdata('error', ' Jenis Barang is already exists ');
				redirect('detail/save/'.$id);

			}
			else
			{ //tidak ada data yang sama

				$data = array(

					'a_ID'			=> $id,
					'd_Name'		=> $file_name,
					'd_Fixed' 		=> $i->post('d_Fixed'),
					'd_Spec'		=> $i->post('d_Spec'),
					'd_Ket'			=> $i->post('d_Ket'),
					'd_Status' 		=> $i->post('d_Status'),
					'd_Date' 		=> $i->post('d_Date'),
					'CreatedBy' 	=> $this->auth_username
				);

				$save = $this->Detail_model->save('tbl_detail', $data);
				if ($save == true){
					$this->session->set_flashdata('success','Detail has been added');
					helper_log("add", "menambahkan detail aset '".$file_name."'", $this->auth_username);
					redirect('detail/index/'.$id);
				}
				else 
				{
					$this->session->set_flashdata('error','Something is wrong!');
					redirect('detail/save/'.$id);
				}	
			}

		}

	}

	public function edit () {
		
		$this->require_min_level(1);
		$id  = $this->uri->segment(3);
		$ida = $this->uri->segment(4);
    	// cek id exist
    	$ci = $this->Detail_model->edit($id);
    	if ($ci != false){

			$this->form_validation->set_rules('d_Name','Computer Name','required');
			$this->form_validation->set_rules('d_Spec','Spesifikasi','required');
			$this->form_validation->set_rules('d_Fixed','No. FIxed Asset','required');
			$this->form_validation->set_rules('d_Ket','Keterangan','required');
			$this->form_validation->set_rules('d_Status','Status','required');

			if($this->form_validation->run() == FALSE) {

	   				$data = array(	

						'title'		=> 'Edit Detail Aset',
						'data' 		=> 	$ci,
						'id'		=> 	$ida,
						'main_view'	=> 'aset/edit_detail'
					);

					$data['stylesheet'] = array(
						"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css",
						"https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"

					);

					$data['javascripts'] = array(
						"https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.full.min.js",
						base_url('assets/js/my.js'),
						"https://code.jquery.com/ui/1.12.1/jquery-ui.js"
					);


					$data['final_script'] = "
					$(document).ready(function() {
		    			$('#date').datepicker({
		    				changeMonth: true,
	 						changeYear: true,
							'dateFormat': 'dd-M-yy',
							'showAnim': 'blind',
							'altField': '#date-alternate',
			  				'altFormat': 'yy-mm-dd'
						});
					});		
					";

	   				$this->breadcrumb->add('Home', site_url('pmis/'));
					$this->breadcrumb->add('Detail', site_url('detail/index/'.$ida));
					$this->breadcrumb->add('Edit', site_url('detail/edit/'.$id.'/'.$ida));
					$this->load->view('themes/template',$data);

	   		}
	   		else 
	   		{

				$id  = $this->uri->segment(3);
				$ida = $this->uri->segment(4);

				$i 	= $this->input;
				
				$file_name = $i->post('d_Name');

				$data = array(

					'a_ID'			=> $ida,
					'd_Name'		=> $file_name,
					'd_Fixed' 		=> $i->post('d_Fixed'),
					'd_Spec'		=> $i->post('d_Spec'),
					'd_Ket'			=> $i->post('d_Ket'),
					'd_Status' 		=> $i->post('d_Status'),
					'd_Date' 		=> $i->post('d_Date'),
					'UpdatedBy' 	=> $this->auth_username
					
				);

				$where = array('d_ID'		=> $id );

				$update = $this->Detail_model->update('tbl_detail', $data, $where);
				if ($update == true){
					$this->session->set_flashdata('success','Detail has been edited');
					helper_log("edit", "mengeditn detail aset '".$file_name."'", $this->auth_username);
				}
				else
				{
					$this->session->set_flashdata('info','Detail still same');
				}
				
				redirect('detail/index/'.$ida);

	    	}
	    }	
    	else
    	{
    		$this->session->set_flashdata('error','Detail does not exists');
    		redirect('detail/index/'.$ida);
    	}
		    
    }

    // Delete
	public function delete() {
		$this->require_min_level(1);
		$id  = $this->uri->segment(3);
		$ida = $this->uri->segment(4);
		//var_dump($id);
		$n = 1;
		$i 	= $this->input;
			
		$data  = array('discard'		=> $n,'UpdatedBy' 	=> $this->auth_username);
		$where = array('d_ID'	=> $id);

		$update = $this->Detail_model->update('tbl_detail', $data, $where);
			if ($update == true):
				$file_name = $this->Detail_model->getDelete($id);
				$this->session->set_flashdata('info', ' Detail has been removed ');
				helper_log("delete", "menghapus detail aset '".$file_name."'", $this->auth_username);
			endif;
			redirect('detail/index/'.$ida);
	}
	
}