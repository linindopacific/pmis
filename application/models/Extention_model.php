<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Extention_model extends CI_model
{

 	var $table = 'telpon';
    //set column field database for datatable orderable
    var $column_order = array(null, 'lt_dep','nm_dep','nm_telp','no_telp'); 
    //set column field database for datatable searchable 
    var $column_search = array('lt_dep','nm_dep','nm_telp','no_telp'); 
    // default order 
    var $order = array('id_telp' => 'asc'); 

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

	//fungsi get_all


    public function getAll()
    {
        $query = $this->db3->select("e_ID as ID, e_Name as Name, e_Ext as Ext, e_Floor as Floor, e_Dept as Dept")
                 ->from('p_extention')
                 ->where('discard', 0)
                 ->order_by('e_Floor', 'Asc')
                 ->order_by('e_Dept', 'Asc')
                 ->order_by('e_Ext', 'Asc')
                 ->get();
        return $query;

    }

    public function edit($id)
    {
        $query = $this->db3->select("e_ID as ID, e_Name as Name, e_Ext as Ext, e_Floor as Floor, e_Dept as Dept")
                 ->from('p_extention')
                 ->where('discard', 0)
                 ->where('e_ID', $id)
                 ->order_by('e_Floor', 'Desc')
                 ->order_by('e_Dept', 'Desc')
                 ->order_by('e_Ext', 'Desc')
                 ->get();
        if($query){
            return $query->row();
        }else{
            return false;
        }

    }

    public function getAllHistory($id)
    {
        $query = $this->db3->select("e_ID as ID, eh_NameN as NameN, eh_ExtN as ExtN, eh_FloorN as FloorN, eh_DeptN as DeptN, eh_NameL as NameL, eh_ExtL as ExtL, eh_FloorL as FloorL, eh_DeptL as DeptL, eh_CreatedAt as CreatedAt, eh_CreatedBy as CreatedBy")
                         ->from('p_extention_h')
                         ->where('e_ID', $id)
                         ->order_by('eh_CreatedAt', 'Desc')
                         ->limit(4)
                         ->get();

        return $query->result();

    }

    public function getid_telp($id)
    {
        return $this->db2->get_where('telpon', array('id_telp' => $id) );
    }

	private function _get_datatables_query()
    {
         
        //add custom filter here
        if($this->input->post('lt_dep'))
        {
            $this->db2->where('lt_dep', $this->input->post('lt_dep'));
        }
        if($this->input->post('nm_dep'))
        {
            $this->db2->like('nm_dep', $this->input->post('nm_dep'));
        }
        if($this->input->post('no_telp'))
        {
            $this->db2->like('no_telp', $this->input->post('no_telp'));
        }
        if($this->input->post('nm_telp'))
        {
            $this->db2->like('nm_telp', $this->input->post('nm_telp'));
        }
 
        $this->db2->from($this->table);
        $i = 0;
     
        foreach ($this->column_search as $item) // loop column 
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                 
                if($i===0) // first loop
                {
                    $this->db2->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db2->like($item, $_POST['search']['value']);
                }
                else
                {
                    $this->db2->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db2->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])) // here order processing
        {
            $this->db2->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } 
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db2->order_by(key($order), $order[key($order)]);
        }
    }
 
    public function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db2->limit($_POST['length'], $_POST['start']);
        $query = $this->db2->get();
        return $query->result();
    }
 
    public function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db2->get();
        return $query->num_rows();
    }
 
    public function count_all()
    {
        $this->db2->from($this->table);
        return $this->db2->count_all_results();
    }
 
    public function get_list_countries()
    {
        $this->db2->select('*');
        $this->db2->from('telpon');
        $this->db2->order_by('id_telp','asc');
        $query = $this->db2->get();
        $result = $query->result();
 
        $lantaii = array();
        foreach ($result as $row) 
        {
            $lantaii[] = $row->lt_dep;
        }
        return $lantaii;
    }

    // Tambah
    public function save ($table, $data) {
        
        $this->db3->insert($table, $data);
        
        if($this->db3->affected_rows()){
            return true;
        }else{
            return false;
        }
    }

    public function update ($table, $data, $where) {
        
        $this->db3->update($table, $data, $where);
        
        if($this->db3->affected_rows()){
            return true;
        }else{
            return false;
        }
    }

 

}