<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pmis_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('tgl_indo');
    }

    public function getAll () {

    	$query = $this->db3->select("a.j_ID as JID, a.j_Name as Jenis, b.a_ID as ID, b.a_Name as Name, b.a_Jumlah as Jmlh, b.a_UOM as UOM, b.a_Location as Loc, b.a_Ket as Ket, b.a_Dept as Dept, b.a_Status as Status, b.a_Entity as Entity, b.a_Fixed as Fixed, b.a_Date as Date")
    					   ->from('tbl_jenis a')
    					   ->join('tbl_asset b','a.j_ID = b.j_ID', 'LEFT')
    					   ->where('b.discard', 0)
    					   ->order_by('ID', 'Desc')
    					   ->get();

   		return $query;
    }

    public function getIDNew () {

        $query = $this->db3->select("a_ID as ID")
                           ->from('tbl_asset')
                           ->where('discard', 0)
                           ->order_by('ID','DESC')
                           ->limit(1)
                           ->get();

        return ($query->num_rows()) ? $query->row()->ID : 0;
    }

    public function getName ($id) {

        $query = $this->db3->select("u_ID as ID, u_Name as Name, u_Date as Date")
                           ->from('tbl_user')
                           ->where('a_ID', $id)
                           ->order_by('ID','DESC')
                           ->get();

        return $query;
    }

    public function getName1 ($id) {

        $query = $this->db3->select("u_Name as Name")
                           ->from('tbl_user')
                           ->where('a_ID', $id)
                           ->order_by('u_ID','DESC')
                           ->limit(1)
                           ->get();

        return ($query->num_rows()) ? $query->row()->Name : 0;
    }

    public function getDate ($id) {

        $query = $this->db3->select("u_Date as Date")
                           ->from('tbl_user')
                           ->where('a_ID', $id)
                           ->order_by('u_ID','DESC')
                           ->limit(1)
                           ->get();

        return ($query->num_rows()) ? $query->row()->Date : 0;
    }

    public function getName2 ($name, $id) {

        $query = $this->db3->select("u_ID as ID, a_ID as AID, u_Name as Name")
                           ->from('tbl_user')
                           ->where('a_ID', $id)
                           ->where('u_Name', $name)
                           ->order_by('ID','DESC')
                           ->get();

        return $query->num_rows();
    }

    public function getDelete ($id) {

      $query = $this->db3->query("SELECT a_Name FROM tbl_asset WHERE a_ID = $id AND discard = 1");
      return  ($query->num_rows()) ? $query->row()->a_Name : 0;

    }

    public function edit ($id) {

        $query = $this->db3->select("a.j_ID as JID, a.j_Name as Jenis, b.a_ID as ID, b.a_Name as Name, b.a_Jumlah as Jmlh, b.a_UOM as UOM, b.a_Location as Loc, b.a_Ket as Ket, b.a_Dept as Dept, b.a_Status as Status, b.a_Entity as Entity, b.a_Fixed as Fixed, b.a_Date as Date  ")
              					   ->from('tbl_jenis a')
              					   ->join('tbl_asset b','a.j_ID = b.j_ID', 'LEFT')
                           ->where('b.discard', 0)
                           ->where('b.a_ID', $id)
                           ->order_by('ID')
                           ->get();

        if($query){
            return $query->row();
        }else{
            return false;
        }
    }

    public function cek_data ($id) {

        $query = $this->db3->select("a_ID as ID, a_Name as Name")
                           ->from('tbl_asset')
                           ->where('discard', 0)
                           ->like('a_Name', $id)
                           ->get();

        return $query->num_rows();
    }

    public function user($user)
    {

      $query = $this->db->select("b.firstname as First, b.middlename as Mid, b.lastname as Last ")
                        ->from('users a')
                        ->join('users_profile b', 'b.user_id = a.user_id', 'LEFT')
                        ->where('a.username', $user)
                        ->where('a.banned','0')
                        ->limit(1)
                        ->get();
      return ($query->num_rows()) ? $query->row()->First.' '.$query->row()->Last : 0;

    }

    public function save ($table, $data) {
        
        $this->db3->insert($table, $data);
        
        if($this->db3->affected_rows()){
            return true;
        }else{
            return false;
        }
    }

    public function update ($table, $data, $where) {
        
        $this->db3->update($table, $data, $where);
        
        if($this->db3->affected_rows()){
            return true;
        }else{
            return false;
        }

    }

    public function save_log($param)
    {
        $sql        = $this->db3->insert_string('log',$param);
        $ex         = $this->db3->query($sql);
        return $this->db3->affected_rows();
    }

 	public function getLog()
    {
        $query = $this->db3->select(" l_ID as ID, l_User as User, l_Time as Time, l_Desc as Desc ")
                          ->from('log')
                          ->order_by('l_Time', 'DESC')
                          ->get();
        return $query->result();
    }  

}