<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Jenis_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->helper('tgl_indo');
    }

    public function getAll () {

    	$query = $this->db3->select("j_ID as ID, j_Name as Name")
    					   ->from('tbl_jenis')
    					   ->where('discard', 0)
    					   ->order_by('ID')
    					   ->get();

   		return $query;
    }

    public function getDelete ($id) {

      $query = $this->db3->query("SELECT j_Name FROM tbl_jenis WHERE j_ID = $id AND discard = 1");
      return  ($query->num_rows()) ? $query->row()->j_Name : 0;

    }

    public function edit ($id) {

        $query = $this->db3->select("j_ID as ID, j_Name as Name")
                           ->from('tbl_jenis')
                           ->where('discard', 0)
                           ->where('j_ID', $id)
                           ->order_by('ID')
                           ->get();

        if($query){
            return $query->row();
        }else{
            return false;
        }
    }

    public function cek_data ($id) {

        $query = $this->db3->select("j_ID as ID, j_Name as Name")
                           ->from('tbl_jenis')
                           ->where('discard', 0)
                           ->where('j_ID', $id)
                           ->order_by('ID')
                           ->get();

        return $query->num_rows();
    }

    // Tambah
    public function save ($table, $data) {
        
        $this->db3->insert($table, $data);
        
        if($this->db3->affected_rows()){
            return true;
        }else{
            return false;
        }
    }

    public function update ($table, $data, $where) {
        
        $this->db3->update($table, $data, $where);
        
        if($this->db3->affected_rows()){
            return true;
        }else{
            return false;
        }

    }
}