<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Detail_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getAll ($id) {

    	$query = $this->db3->select("d_ID as ID, d_Fixed as Fixed, a_ID as AID, d_Name as Name, d_Spec as Spec, d_Ket as Ket, d_Status as Status, d_Date as Date")
    					   ->from('tbl_detail')
    					   ->where('discard', 0)
                           ->where('a_ID',$id)
    					   ->order_by('ID')
    					   ->get();

   		return $query;
    }

    public function getDelete ($id) {

      $query = $this->db3->query("SELECT d_Name FROM tbl_detail WHERE d_ID = $id AND discard = 1");
      return  ($query->num_rows()) ? $query->row()->d_Name : 0;

    }

    public function getuser ($id) {

      $query = $this->db3->query("SELECT u_Name FROM tbl_user WHERE a_ID = $id ORDER BY u_ID DESC LIMIT 1");
      return  ($query->num_rows()) ? $query->row()->u_Name : 0;

    }

    public function getAset ($id) {

        $query = $this->db3->select("a_ID as ID, a_Name as Name, a_Jumlah as Jmlh, a_UOM as UOM, a_Location as Loc, a_Ket as Ket, a_Dept as Dept, a_Status as Status, a_Entity as Entity, A_Fixed as Fixed, a_Date as Date ")
                           ->from('tbl_asset')
                           ->where('discard', 0)
                           ->where('a_ID', $id)
                           ->order_by('ID')
                           ->get();

        return $query->row();
    }

    public function edit ($id) {

        $query = $this->db3->select("d_ID as ID, d_Fixed as Fixed, a_ID as AID, d_Name as Name, d_Spec as Spec, d_Ket as Ket, d_Status as Status, d_Date as Date")
                           ->from('tbl_detail')
                           ->where('discard', 0)
                           ->where('d_ID', $id)
                           ->get();

        if($query){
            return $query->row();
        }else{
            return false;
        }
    }

    public function cek_data ($id) {

        $query = $this->db3->select("d_ID as ID, d_Name as Name")
                           ->from('tbl_detail')
                           ->where('discard', 0)
                           ->where('d_Name', $id)
                           ->order_by('ID')
                           ->get();

        return $query->num_rows();
    }

    // Tambah
    public function save ($table, $data) {
        
        $this->db3->insert($table, $data);
        
        if($this->db3->affected_rows()){
            return true;
        }else{
            return false;
        }
    }

    public function update ($table, $data, $where) {
        
        $this->db3->update($table, $data, $where);
        
        if($this->db3->affected_rows()){
            return true;
        }else{
            return false;
        }

    }
}