<script type="text/javascript">
function show_hide_row(row)
{
 $("#"+row).toggle();
}
</script>

<div class="container" style="margin-top: 5px; margin-right: 0px; margin-left: 0px; max-width: 500%; font-size: 11px; ">
	
	<div>
		<a role="button" href="<?php echo site_url('pmis/save')?>" class="btn btn-primary"><span class="btn-label"><i class="fa fa-plus"></i></span>New</a>
		<p>	
	</div>
	
		<!-- table -->
		<div class="table-responsive" style="padding-right: 5px;">
			<table id="table" class="table table-striped table-bordered table-hover " width="100%" cellpadding="0" cellspacing="0">
			    <thead>
			      <tr style="background-color: #428bca; color: #FFFFFF;">
			        <th width="1%"><center>No</center></th>
					<th><center>Jenis Aset</center></th>
					<th><center>Nama Aset</center></th>
					<th><center>Jumlah Aset</center></th>
					<th width="1%"><center>UOM Aset</center></th>
					<th><center>Lokasi Aset</center></th>
					<th><center>Departemen Aset</center></th>
					<th><center>Keterangan aset</center></th>
					<th><center>Status Aset</center></th>
					<th><center>Tgl Beli Aset</center></th>
					<th width="1%"><center>Aksi</center></th>
			      </tr>
			    </thead>
			    <tbody>
				<?php
				$i = 1;
				foreach($data->result() as $h) {
				
				$hidden_row= "hidden_row$i";
				?>
					<tr onclick="show_hide_row('<?=$hidden_row?>');">
						<td align= "center"><?= $i++; ?></td>
						<td align= "center"><?= $h->Jenis; ?></td>
						<td align= "center" nowrap><?= $h->Name;?></td>
				        <td align= "center" nowrap><?= $h->Jmlh; ?></td>
				        <td align= "center" nowrap><?= $h->UOM;?></td>
				        <td align= "center" nowrap><?= $h->Loc;?></td>
				        <td align= "center" nowrap><?= $h->Dept;?></td>
				        <td align= "center" nowrap><?= $h->Ket; ?></td>
				        <td align= "center" nowrap><?= $h->Status;?></td>
				        <td align= "center" nowrap><?= date_indo1($h->Date);?></td>
				        <td align= "center">
							<a href="<?php echo site_url('pmis/edit')?>/<?= $h->ID ?>" title="Edit"><i class="fa fa-fw fa-pencil-square" style="color: #000000;"></i></a>
							<a href="<?php echo site_url('pmis/delete')?>/<?= $h->ID ?>" title="Delete" onclick="return confirm('Are you sure you want to delete?')"><i class="fa fa-fw fa-window-close" style="color: #000000;"></i></a>
							<a href="<?php echo site_url('detail/index')?>/<?= $h->ID ?>" title="Detail Aset"  ><i class="fa fa-fw fa-folder-open" style="color: #000000;"></i></a>
						</td>  
				    </tr>
				    <tr id="<?=$hidden_row?>" class="hidden_row" style="display:none;">
						<td colspan="10">
							<table class="table table-striped table-bordered table-hover sortable"  style=" width : 50%;" cellpadding="0" cellspacing="0">
								<tr style="background-color: #008B8B;color: #FFFFFF;">
									<th><center>Entity</center></th>
									<th><center>User</center></th>
									<th><center>Tanggal Mulai Digunakan</center></th>
								</tr>
									<?php 
									$query = $this->Pmis_model->getName($h->ID);
									foreach($query->result() as $y) {
										if($y->Date == "0000-00-00"){
											$tgl = "";
										}
										else
										{
											$tgl = date_indo1($y->Date);
										}
										 
									?>
								<tr>
							        <td align= "center"><?php 							        	
							        	$entity = $this->entity->getEntityByPrefix($h->Entity);
										echo $entity->entity_name;
									?>
							        </td>
							        <td align= "center"><?= $y->Name ?></td>   
							        <td align= "center"><?= $tgl ?></td>   
							    </tr>
							<?php } ?>
							</table>
						</td>	    
					</tr> 
				<?php } ?>
			    </tbody>
			 </table>
		</div>

</div>