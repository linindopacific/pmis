	

<div class="container" style="margin-top: 5px; margin-right: 0px; margin-left: 0px; max-width: 100%; font-size: 11px; ">
	
	<div>
		<a role="button" href="<?php echo site_url('pmis')?>" class="btn btn-primary"><span class="btn-label"><i class="fa fa-chevron-circle-left"></i></span>BACK</a>
		<p>	
	</div>

	<table class="table " style="width: 70%;" width="70%" cellpadding="0" cellspacing="0">
		<tr>
			<td style="width: 15%; background-color: #428bca; color: #FFFFFF;">Computer Name</td>
			<td style="width: 1%; background-color: #428bca; color: #FFFFFF;"><center>:</center></td>
			<td style="width: 29%; background-color: #FFFFFF;"><?= $data->Name ?></td>
			<td style="width: 10%; border: 0;"></td>
			<td style="width: 15%; background-color: #428bca; color: #FFFFFF;">User</td>
			<td style="width: 1%; background-color: #428bca; color: #FFFFFF;"><center>:</center></td>
			<td style="width: 29%; background-color: #FFFFFF;"><?= $pic = $this->Detail_model->getuser($data->ID); ?></td>
		</tr>
		<tr>
			<td style="width: 15%; background-color: #428bca; color: #FFFFFF;">Entity</td>
			<td style="width: 1%; background-color: #428bca; color: #FFFFFF;"><center>:</center></td>
			<td style="width: 29%; background-color: #FFFFFF;"><?php 
				$entity = $this->entity->getEntityByPrefix($data->Entity);
				echo $entity->entity_name;
			?>				
			</td>
			<td style="width: 10%;  border: 0;"></td>
			<td style="width: 15%; background-color: #428bca; color: #FFFFFF;">No. Fixed Aset</td>
			<td style="width: 1%; background-color: #428bca; color: #FFFFFF;"><center>:</center></td>
			<td style="width: 29%; background-color: #FFFFFF;"><?= $data->Fixed ?></td>
		</tr>
	</table>

	<div>
		<a role="button" href="<?php echo site_url('detail/save')?>/<?= $id ?>" class="btn btn-primary"><span class="btn-label"><i class="fa fa-plus-square"></i></span>ADD DETAIL</a>
		<p>	
	</div>
	
		<!-- table -->
		<div class="table-responsive" style="padding-right: 5px;">
			<table id="table" class="table table-striped table-bordered table-hover " width="100%" cellpadding="0" cellspacing="0">
			    <thead>
			      <tr style="background-color: #428bca; color: #FFFFFF;">
			        <th width="1%"><center>No</center></th>
			        <th><center>No. Fixed Aset</center></th>
					<th><center>Nama Hardware</center></th>
					<th><center>Spesifikasi</center></th>
					<th><center>Lokasi</center></th>
					<th><center>Departemen</center></th>
					<th><center>Keterangan</center></th>
					<th><center>Status</center></th>
					<th><center>Tgl Beli Hardware</center></th>
					<th width="1%"><center>Aksi</center></th>
			      </tr>
			    </thead>
			    <tbody>
			    <?php 
			    $i = 1;
				foreach($data1->result() as $h) {

			    ?>
			    	<tr>
			    		<td align= "center"><?= $i++; ?></td>
						<td align= "center"><?= $h->Fixed; ?></td>
						<td align= "center"><?= $h->Name; ?></td>
						<td align= "center"><?= $h->Spec; ?></td>
						<td align= "center"><?= $data->Loc; ?></td>
						<td align= "center"><?= $data->Dept; ?></td>
						<td align= "center"><?= $h->Ket; ?></td>
						<td align= "center"><?= $h->Status; ?></td>
						<td align= "center"><?= date_indo1($h->Date); ?></td>
						<td align= "center">
							<a href="<?php echo site_url('detail/edit')?>/<?= $h->ID ?>/<?= $id ?>" title="Edit"><i class="fa fa-fw fa-pencil-square" style="color: #000000;"></i></a>
							<a href="<?php echo site_url('detail/delete')?>/<?= $h->ID ?>/<?= $id ?>" title="Delete" onclick="return confirm('Are you sure you want to delete?')"><i class="fa fa-fw fa-window-close" style="color: #000000;"></i></a>
						</td>
			    	</tr>
			    <?php } ?>
			    </tbody>
			 </table>
		</div>

</div>