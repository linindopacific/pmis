<div class="col-md-12">
 <?php echo form_open('',['autocomplete'=>'off']) ?>
		
	
	<div class="row">
	  	
	  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
		  <div class="card mb-3">
			
			<div class="card-header">
				<h3><i class="fa fa-file-text-o"></i> Data Aset</h3>
			</div>

			<div class="card-body">

				<div class="row">
					
					<div class="col-lg-4"> 
		      	  		<div class="form-group">
							<label>Jenis Aset</label><?=form_error('j_ID', '<span class="badge badge-danger">','</span>'); ?>
							<div class="col-12" style="max-width: 100%;">
								<select class="form-control select2 custom-select d-block my-3" required name="j_ID">
									<option value="" disabled selected>--pilih--</option>
									<?php 
										foreach ($data->result_array() as $h) {
										echo "<option value='$h[ID]'>$h[Name]</option>";
									}?>
								</select>
					  		</div>
				  		</div>
					</div>

				  	<div class="col-lg-4"> 
		      	  		<div class="form-group">
							<label>Nama Aset</label><?=form_error('a_Name', '<span class="badge badge-danger">','</span>'); ?>
							<div class="col-12" style="max-width: 100%;">
								<input name="a_Name" type="text" class="form-control form-control-sm" placeholder="Masukkan nama aset.." >
					  		</div>
				  		</div>
					</div>

					<div class="col-lg-4"> 
		      	  		<div class="form-group">
							<label>No. Fixed Aset</label>
							<div class="col-12" style="max-width: 100%;">
								<input name="a_Fixed" type="text" class="form-control form-control-sm" placeholder="Masukkan Fixed.." >
					  		</div>
				  		</div>
					</div>

				</div>

				<div class="row">
					
					<div class="col-lg-6"> 
		      	  		<div class="form-group">
							<label>Entity</label><?=form_error('a_Entity', '<span class="badge badge-danger">','</span>'); ?>
							<div class="col-12" style="max-width: 100%;">
								<select class="form-control select2 custom-select d-block my-3" required name="a_Entity">
									<option value="" disabled selected>--Pilih Entity--</option>
									<option value="PAI">Paz Ace Indonesia</option>
									<option value="PMP">Persada MultiParts</option>
									<option value="DLT">Dwikarya Linindo Teknik</option>
									<option value="ASI">Allight Sykes Indonesia</option>
									<option value="ARN">Aruna International</option>
									<option value="LTI">Linindo Teknik International</option>
									<option value="MTI">Multipedia Teknika Indonesia</option>							
								</select>
					  		</div>
				  		</div>
					</div>

					<div class="col-lg-6"> 
		      	  		<div class="form-group">
							<label>Tanggal Beli Aset</label><?=form_error('a_Date', '<span class="badge badge-danger">','</span>'); ?>
							<div class="col-12" style="max-width: 100%;">
								<input id="date1" type="text" class="form-control form-control-sm" requiered placeholder="Input tanggal beli aset..">
			    				<input id="date1-alternate" type="hidden" name="a_Date" class="form-control" >
					  		</div>
				  		</div>
					</div>		  	

				</div>

				<div class="row">

		        	<div class="col-lg-6"> 
		      	  		<div class="form-group">
							<label>Jumlah aset</label><?=form_error('a_Jmlh', '<span class="badge badge-danger">','</span>'); ?>
							<div class="col-12" style="max-width: 100%;">
					  			<input name="a_Jmlh" type="number" class="form-control form-control-sm" placeholder="Masukkan jumlah aset..." style="text-align:right;" >
							</div>
				  		</div>
					</div>

					<div class="col-lg-6"> 
		      	  		<div class="form-group">
							<label>UOM</label><?=form_error('a_UOM', '<span class="badge badge-danger">','</span>'); ?>
							<div class="col-12" style="max-width: 100%;">
								<input name="a_UOM" type="text" class="form-control form-control-sm" placeholder="Masukkan UOM aset" >
					  		</div>
				  		</div>
					</div>

			 	</div>

			 	<div class="row">

		        	<div class="col-lg-6"> 
		      	  		<div class="form-group">
							<label>Lokasi</label><?=form_error('a_Location', '<span class="badge badge-danger">','</span>'); ?>
							<div class="col-12" style="max-width: 100%;">
					  			<input name="a_Location" type="text" class="form-control form-control-sm" placeholder="Masukkan jumlah aset..." >
							</div>
				  		</div>
					</div>

					<div class="col-lg-6"> 
		      	  		<div class="form-group">
							<label>Departemen</label><?=form_error('a_Dept', '<span class="badge badge-danger">','</span>'); ?>
							<div class="col-12" style="max-width: 100%;">
								<input name="a_Dept" type="text" class="form-control form-control-sm" placeholder="Masukkan UOM aset" >
					  		</div>
				  		</div>
					</div>
					
			 	</div>

			 	<div class="row">
				  	<div class="col-lg-6"> 
		      	  		<div class="form-group">
							<label>Keterangan</label>
							<div class="col-12" style="max-width: 100%;">
								<input name="a_Ket" type="text" class="form-control form-control-sm" placeholder="Masukkan keterangan aset.." >
					  		</div>
				  		</div>
					</div>

					<div class="col-lg-6"> 
		      	  		<div class="form-group">
							<label>Status</label><?=form_error('a_Status', '<span class="badge badge-danger">','</span>'); ?>
							<div class="col-12" style="max-width: 100%;">
								<input name="a_Status" type="text" class="form-control form-control-sm" placeholder="Masukkan status aset.." >
					  		</div>
				  		</div>
					</div>
			 	</div>
			 	
			</div>

		  </div>
	  	</div>
	</div>

	<div class="row">
	  	
	  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
		  	<div class="card mb-3">
			
				<div class="card-header">
					<h3><i class="fa fa-file-text-o"></i> Data User Aset</h3>
				</div>

				<div class="card-body">
					<div class="row">

						<div class="col-lg-6"> 
			      	  		<div class="form-group">
								<label>User</label><?=form_error('a_PIC', '<span class="badge badge-danger">','</span>'); ?>
								<div class="col-12" style="max-width: 100%;">
									<input name="a_PIC" type="text" class="form-control form-control-sm" placeholder="Masukkan user aset.." >
						  		</div>
					  		</div>
						</div>

						<div class="col-lg-6"> 
			      	  		<div class="form-group">
								<label>Tanggal Mulai Pemakaian</label><?=form_error('u_Date', '<span class="badge badge-danger">','</span>'); ?>
								<div class="col-12" style="max-width: 100%;">
									<input id="date2" type="text" class="form-control form-control-sm" requiered placeholder="Input tanggal mulai pemakai aset..">
			    					<input id="date2-alternate" type="hidden" name="u_Date" class="form-control" >
						  		</div>
					  		</div>
						</div>

					</div>
				</div>

			</div>
		</div>

	</div>

	<p></p>

		<div>
			<button type="submit" id="btnSubmit" class="btn btn-md btn-success">SAVE</button>
			<button type="reset" class="btn btn-md btn-danger">RESET</button>
			<a href="<?php echo site_url('pmis/')?>" class="btn btn-md btn-info">BACK</a>
			<?php echo form_close() ?>
		</div>

</div>
