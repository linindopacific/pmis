<div class="col-md-12">
 <?php echo form_open('',['autocomplete'=>'off']) ?>
		
	
	<div class="row">
	  	
	  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
		  <div class="card mb-3">
			
			<div class="card-header">
				<h3><i class="fa fa-file-text-o"></i> Detail Hardware</h3>
			</div>

			<div class="card-body">

				<div class="row">
					
					<div class="col-lg-4"> 
		      	  		<div class="form-group">
							<label>No. Fixed Aset</label><?=form_error('d_Fixed', '<span class="badge badge-danger">','</span>'); ?>
							<div class="col-12" style="max-width: 100%;">
								<input name="d_Fixed" type="text" class="form-control form-control-sm" value="<?= $data->Fixed ?>">
					  		</div>
				  		</div>
					</div>

				  	<div class="col-lg-4"> 
		      	  		<div class="form-group">
							<label>Nama Hardware</label><?=form_error('d_Name', '<span class="badge badge-danger">','</span>'); ?>
							<div class="col-12" style="max-width: 100%;">
								<input name="d_Name" type="text" class="form-control form-control-sm" placeholder="Masukkan nama hardware.." >
					  		</div>
				  		</div>
					</div>

					<div class="col-lg-4"> 
		      	  		<div class="form-group">
							<label>Tanggal Beli Hardware</label><?=form_error('d_Name', '<span class="badge badge-danger">','</span>'); ?>
							<div class="col-12" style="max-width: 100%;">
								<input id="date" type="text" class="form-control form-control-sm" requiered placeholder="Input tanggal mulai pemakai aset..">
			    				<input id="date-alternate" type="hidden" name="d_Date" class="form-control" >
					  		</div>
				  		</div>
					</div>

				</div>

				<div class="row">

		        	<div class="col-lg-4"> 
		      	  		<div class="form-group">
							<label>Spesifikasi</label><?=form_error('d_Spec', '<span class="badge badge-danger">','</span>'); ?>
							<div class="col-12" style="max-width: 100%;">
					  			<input name="d_Spec" type="text" class="form-control form-control-sm" placeholder="Masukkan spesifikasi.." >
							</div>
				  		</div>
					</div>

					<div class="col-lg-4"> 
		      	  		<div class="form-group">
							<label>Keterangan</label><?=form_error('d_Ket', '<span class="badge badge-danger">','</span>'); ?>
							<div class="col-12" style="max-width: 100%;">
								<input name="d_Ket" type="text" class="form-control form-control-sm" placeholder="Masukkan Keterangan.." >
					  		</div>
				  		</div>
					</div>

					<div class="col-lg-4"> 
		      	  		<div class="form-group">
							<label>Status</label><?=form_error('d_Status', '<span class="badge badge-danger">','</span>'); ?>
							<div class="col-12" style="max-width: 100%;">
								<input name="d_Status" type="text" class="form-control form-control-sm" placeholder="Masukkan status.." >
					  		</div>
				  		</div>
					</div>

			 	</div>
			 	
			</div>

		  </div>
	  	</div>
	</div>

	<p></p>

		<div>
			<button type="submit" id="btnSubmit" class="btn btn-md btn-success">SAVE</button>
			<button type="reset" class="btn btn-md btn-danger">RESET</button>
			<a href="<?php echo site_url('detail/index/')?><?= $id?>" class="btn btn-md btn-info">BACK</a>
			<?php echo form_close() ?>
		</div>

</div>