<div class="col-md-12">
 <?php echo form_open('',['autocomplete'=>'off']) ?>		
	

	<div class="row">

	  	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">						
		  	<div class="card mb-3">
			
				<div class="card-header">
					<h3><i class="fa fa-file-text-o"></i> Jenis Barang</h3>
				</div>

				<div class="card-body">

					<div class="row">
					  	<div class="col-lg-4"> 
			      	  		<div class="form-group">
								<label>Nama Jenis</label><?=form_error('j_Name', '<span class="badge badge-danger">','</span>'); ?>
								<div class="col-12" style="max-width: 100%;">
									<input name="j_Name" type="text" class="form-control form-control-sm" value="<?= $data->Name; ?>" >
						  		</div>
					  		</div>
						</div>
			        </div>

			    </div>	

		  	</div>
	  	</div>

	</div>	        	

	<p></p>

		<div>
			<button type="submit" id="btnSubmit" class="btn btn-md btn-success">SAVE</button>
			<button type="reset" class="btn btn-md btn-danger">RESET</button>
			<a href="<?php echo site_url('jenis/')?>" class="btn btn-md btn-info">BACK</a>
			<?php echo form_close() ?>
		</div>

</div>