<div class="container" style="margin-top: 5px; margin-right: 0px; margin-left: 0px; max-width: 100%; font-size: 11.5px; ">

	<div class="row">
		
		<div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
				<div class="card-box noradius noborder bg-info">
						<a href="<?php echo site_url('kendaraan/')?>"><i class="fa fa-car float-right text-white"></i></a>
						<h6 class="text-white text-uppercase m-b-20">Data Kendaraan</h6>
						<h1 class="m-b-20 text-white counter"><?php echo $kendaraan;?></h1>
						<h6 class="text-white">Kendaraan</h6>
				</div>
		</div>

		<div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
				<div class="card-box noradius noborder bg-success">
						<a href="<?php echo site_url('extention/')?>"><i class="fa fa-phone-square float-right text-white"></i></a>
						<h6 class="text-white text-uppercase m-b-20">Data Extention</h6>
						<h1 class="m-b-20 text-white counter"><?php echo $extention;?></h1>
						<h6 class="text-white">Ext</h6>
				</div>
		</div>

		<div class="col-xs-12 col-md-6 col-lg-6 col-xl-3">
				<div class="card-box noradius noborder bg-warning">
						<a href="<?php echo site_url('rumah/')?>"><i class="fa fa-building float-right text-white"></i></a>
						<h6 class="text-white text-uppercase m-b-20">Data Sewa</h6>
						<h1 class="m-b-20 text-white counter"><?php echo $rumah;?></h1>
						<h6 class="text-white">Bangunan</h6>
				</div>
		</div>


	</div>
	<!-- end row -->

<div>