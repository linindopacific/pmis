<!-- Left Sidebar -->
    <div class="left main-sidebar">
    
        <div class="sidebar-inner leftscroll">

            <div id="sidebar-menu">
        
            <ul>

                    <li class="submenu">
                        <a href="<?php echo base_url('index.php/pmis') ?>"><i class="fa fa-fw fa-home"></i><span> Home </span> </a>
                    </li>
                                        
                    <li class="submenu">
                        <a href="#"><i class="fa fa-fw fa-drivers-license-o "></i> <span> Jenis Aset </span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="<?php echo base_url('index.php/jenis/') ?>"><i class="fa fa-fw fa-arrow-circle-o-right"></i>Data Jenis</a></li>
                            </ul>
                    </li>

                    <li class="submenu">
                        <a href="#"><i class="fa fa-fw fa-history "></i> <span> Log </span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="<?php echo base_url('index.php/pmis/log') ?>"><i class="fa fa-fw fa-arrow-circle-o-right"></i>Show Log</a></li>
                            </ul>
                    </li>
                    
            </ul>

            <div class="clearfix"></div>

            </div>
        
            <div class="clearfix"></div>

        </div>

    </div>
    <!-- End Sidebar -->